# -*- coding: utf-8 -*-
# from __future__ import absolute_import, unicode_literals
from debug_toolbar.panels.templates import TemplatesPanel as BaseTemplatesPanel

class TemplatesPanel(BaseTemplatesPanel):
    def generate_stats(self, *args):
        if len(self.templates) > 0:
            template = self.templates[0]['template']
            if not hasattr(template, 'engine') and hasattr(template, 'backend'):
                template.engine = template.backend
            return super(TemplatesPanel, self).generate_stats(*args)
        else:
            return None

