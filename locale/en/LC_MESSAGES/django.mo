��    Z      �     �      �  �  �     �
     �
  �   �
     L  <   S     �     �     �     �     �     �  	   �     �     �                    7     C     Q     _     n     {     �     �  j   �     
                    "     +     0     6  	   D     N     S     \     c     h     v     ~     �     �     �     �     �     �  	   �     �     �     �     �     �        
              4     C     P     ]     b     i  
   r     }  	   �  9   �     �     �     �  	               	   *     4  '   =  #   e      �  	   �     �  R   �  	             )     >  	   E     O  @   i  �  �  x  >     �     �  a   �     3  U   ;     �     �     �     �     �     �  
   �     �     �  	   �  	          
   0     ;     H  
   U  	   `     j     o     �  P   �  
   �     �  
   �     �  	   �                              &     -     6     ;     H     N     ]     m     |     �     �     �     �     �  	   �  
   �     �     �     �     �     �     �               -     5     :     @     H     Q  5   Z     �     �     �     �     �     �     �     �  #   �          2     Q     X  H   `  
   �     �     �     �  	   �     �  F   �               (      ,       R           -      F   N   $           "      1   D   ;   0   :              B      Z                    3      S   !   4   <   I      T   Q   V                  
   .           W       ?   '       >       6       J       L                        K       G   H   X   O                  E         +         P   @   	   *      =   C   5           A   #   7   Y              U             /   9       )                  M          &   %   2          8    
                    <p>
                        Hér má koma á framfæri athugasemdum eða hugmyndum varðandi rekstur og starfsemi Ölfusborga.
                    </p>
                    <p>
                        Einnig væri gaman að fá stuttar frásagnir um dvöl í Ölfusborgum eða annað sem heyrir til sögu Ölfusborga.
                    </p>
                    <p>
                        Hægt er að setja allt að 5 myndir samtímis á upphleðsluflötinn en það er hægt að senda fleirei meyndir með skilaboðunum.<br>
                        Gott er að skýra innihalds þeirra, stað og geta þeirra sem á myndumun eru og höfunda í texta.
                    </p>
                  er ekki gilt netfang Afgreitt Athugasemdir eða hugmyndir varðandi starfsemi Ölfusborga er hægt að koma á framfæri með því að smella á talblöðrurnar neðst til hægri á vefsíðunni. Breytt Ef skilaboðin varða tiltekið hús veldu það af listanum Eigandi færslu Einkunn Einstaklingar Einstaklingur Enska Erfið Forsíða Fyrirtæki eða stofnanir Fyrirtæki eða stofnun Grannar Grannar  Grannar og aðstandendur Grunneining Grunneiningar Gögnuleiðir Gönduslóðir Gönguslóð Heiti Hvert er erindið Hætta við Hér er hægt að senda athugasemdir, hugmyndir eða tilkynningar um eitthvað sem gæti verið í ólagi. Hús Hús:  Húsin Kort Loftmynd Loka Létt Minnispunktar Miðlungs Mynd Myndefni Myndir Nafn Nafn sendanda Netfang Netfang sendanda Nokkuð létt Nokuð erfið Nánar Saga Senda Sjáskot komið Skilaboð Skjal Skjöl Skráð Staðsetning Stikla Stiklur Stillingar Svipmyndir úr sögu Ölfusborga Sími sendanda Sögustubbar Sögustubbur Takk Tegund Tegundir Tilkynning Tilkynningar Tungumál Umsjón og tæknilegur reksur vefsins er í hans umsjón. Upplýsingaflokkar Upplýsingaflokkur Upplýsingar Varðandi Varðar Varðar þetta ekkert? Vefsíða Vefurinn Vefurinn er unninn af Einari Bergmundi. Veldu mynd(ir) eða dragðu hingað Veldu skrá eða dragðu hingað Venjulegt Veður Vinsamlega látið netfang fylgja svo hægt sé að hafa samband sé þess þörf. Viðhengi Viðhengi komið skilaboð móttekin. Ágrip Íslenska Það eru engin skilaboð Þú verður að staðfesta mennsku og smella á Captcha kassann Project-Id-Version: olfus
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-03-23 20:12+0000
PO-Revision-Date: 2017-03-23 20:23+0000
Last-Translator: Náttúran er ehf <nature@nature.is>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 
                    <p>
                        Here you can send us observations or ideas regarding Ölfusborgir.
                    </p>
                    <p>
                        We would also like to get some stories about the life in Ölfusborgir in past and present.
                    </p>
                    <p>
                        You can drag or add up to 5 images at the time to the upload area, but there can be more images in the message.<br>
                        It would be good to have some description of the content of the image, people on it, author and approximate date .
                    </p> is not a valid email Done In case you want to comment on Ölfusborgir you can click on the talk-bubbles down to the right.  Changed If the message is regarding a specific house, please select it's number from the list Record owner Grade Persons Person English Tough Front Page Institutions or companies Institution or company Neighbors Neighbors Neighbors and related entities Base clsss Base classes Hiking paths Hike paths Hike path Name What is this about Cancel In case of a problem, question, suggestion or praise a message can be sent here. The Houses House:  The Houses Map Sattelite Close Easy Notes Medium Picture Images Pictures Name Senders name Email Senders e-mail Relatively easy Somewhat tough Details History Send Screenshot ready Message Document Documents Registered Location Settings Teaser Settings Glimps from history Senders phone Historical stubs Historical stub Thanks  Type Types Message Messages Language He takes care of technical matters regarding the web. Information Information Information Subject Subject Is this about nothing? Website The Website The web is made by Einar Bergmundur Select a file or drag one here Select a file or drag one here Normal Weather Please include your e-mail for response, it will not be given to anyone. Attachment Attachement ready message received  Synopsis Icelandic There is no message You have to confirm that you are not a robot and click the Captcha box 