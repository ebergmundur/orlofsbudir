"""orlofsbudir URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.staticfiles import views
from django.conf.urls.i18n import i18n_patterns
# from jet.dashboard.dashboard_modules import yandex_metrika_views
from jet.dashboard.dashboard_modules import google_analytics_views
from orlofsbudir import settings
from django.views.decorators.cache import cache_page


urlpatterns = [
    url(r'^filer/', include('filer.urls')),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^', include('olfus.urls', namespace='olfus')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # url(r'^admin/', include(admin.site.urls)),
    # url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),


]

# urlpatterns += i18n_patterns(
#     url(r'^', include('olfus.urls', namespace='olfus')),
#     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
#     url(r'^admin/', include(admin.site.urls)),
#     # url(r'^about/$', about_views.main, name='about'),
#     # url(r'^news/', include(news_patterns, namespace='news')),
# )

from django.conf.urls.static import static

#if settings.DEBUG:
if True:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^static/(?P<path>.*)$', views.serve),
        url(r'^media/(?P<path>.*)$', views.serve),
    ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()

