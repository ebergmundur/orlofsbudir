"""
WSGI config for orlofsbudir project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os, socket

from django.core.wsgi import get_wsgi_application

srv = socket.gethostname()

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "illugastadir.settings")

if srv == 'verk':
	import newrelic.agent
#	newrelic.agent.initialize( '/usr/local/djangoprojects/OLFUS/orlofsbudir/newrelic_orlof.ini' )

application = get_wsgi_application()




# import os, socket
# import logging
# from django.core.wsgi import get_wsgi_application
#
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nature3.settings.settings")
#
# logger = logging.getLogger(__name__)
#
# srv = socket.gethostname()
#
# if srv == 'verk':
# 	import newrelic.agent
# 	newrelic.agent.initialize( '/usr/local/djangoprojects/nature3/newrelic-nature.ini' )
#
#
# application = get_wsgi_application()
#
# logger.info('WSGI STARTUP - - - - - - - - - - - - -')
# logger.info(application)
# logger.info(srv)


