from django import template
from django.utils import timezone
from olfus.forms import MessageForm, TechNoteForm
from olfus.models import Announcement, Settings

register = template.Library()


@register.simple_tag(takes_context=True)
def get_announcement_list(context, lang):
    curr = timezone.now()
    announcements = Announcement.objects.filter(active_date__lte=curr, inactive_date__gte=curr)
    if announcements.count() > 0:
        return announcements
    return None


@register.simple_tag(takes_context=True)
def get_message_form(context):
    messageform = MessageForm()
    return messageform



@register.simple_tag(takes_context=True)
def get_webtech_form(context):
    webtechform = TechNoteForm()
    return webtechform



@register.simple_tag(takes_context=True)
def get_header(context):
    # curr = timezone.now()
    header = Settings.objects.filter(active=True)
    if header.count() > 0:
        return header.first()
    else:
        header = Settings.objects.filter(default=True)
        if header.count() > 0:
            return header.first()
    return None





def in_lang(object, lang):
    """Retruns translated text of object."""

    for islang in object.lang:
        if islang.language == lang:
            return islang

    for islang in object.lang:
        if islang.language == 'en':
            return islang

    for islang in object.lang:
        if islang.language == 'is':
            return islang


register.filter('in_lang', in_lang)
