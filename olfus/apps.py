from django.apps import AppConfig


class OlfusConfig(AppConfig):
    name = 'olfus'
