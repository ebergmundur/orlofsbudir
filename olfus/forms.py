from django import forms
from snowpenguin.django.recaptcha2.fields import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget
from olfus.models import Message, MessageFile, MessageImage, TechNote


TO_HIDE_ATTRS = {'class': 'hidden'}

class MessageForm(forms.ModelForm):
    captcha = ReCaptchaField(widget=ReCaptchaWidget())
    messageimageattachements = forms.CharField(widget=forms.HiddenInput())
    # explicit = True
    class Meta:
        model = Message
        fields = [
            'title',
            'house',
            'sender_name',
            'sender_phone',
            'sender_email',
            'message',
            'picture',
            'rating',
        ]
        widgets = {
            'rating': forms.HiddenInput(),
        }


class TechNoteForm(forms.ModelForm):
    # captcha = ReCaptchaField(widget=ReCaptchaWidget())

    class Meta:
        model = TechNote
        fields = [
            'wt_subject',
            'wt_name',
            'wt_email',
            'wt_note',
            'wt_screenshot',
        ]


class MessageImageForm(forms.Form):
    image = forms.ImageField()
