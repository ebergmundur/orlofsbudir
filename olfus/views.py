from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

from .models import History, Announcement, Person, Institution, House, HikePath, Document, Image, Help, HelpCategory, \
    Message, MessageImage, TechNote
from .serializers import HikePathSerializer, HouseSerializer, InstitutionSerializer, HistorySerializer
from django.shortcuts import render
from rest_framework import generics
from .forms import MessageForm, MessageImageForm, TechNoteForm
from django.core.mail import send_mail
from django.http import JsonResponse, HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import json
from django.views.decorators.cache import cache_page

# Create your views here.
class HikePathList(generics.ListCreateAPIView):
    queryset = HikePath.objects.filter(active=True)
    serializer_class = HikePathSerializer


class HikePathDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = HikePath.objects.all()
    serializer_class = HikePathSerializer


class HouseList(generics.ListCreateAPIView):
    queryset = House.objects.filter(active=True)
    serializer_class = HouseSerializer


class HouseDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = House.objects.all()
    serializer_class = HouseSerializer


class InstitutionList(generics.ListCreateAPIView):
    queryset = Institution.objects.filter(active=True, on_service_list=True)
    serializer_class = InstitutionSerializer


class InstitutionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Institution.objects.all()
    serializer_class = InstitutionSerializer


class HistoryList(generics.ListCreateAPIView):
    queryset = History.objects.filter(active=True)
    serializer_class = HistorySerializer


class HistoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = History.objects.filter(active=True)
    serializer_class = HistorySerializer


# @login_required()
# @cache_page(60 * 15)
def base(request):
    curr = timezone.now()
    announcements = Announcement.objects.filter(active_date__lte=curr, inactive_date__gte=curr)
    historyevents = History.objects.filter(active=True).order_by('?')[:4]

    return render(request, 'base.html', {
        'announcements': announcements,
        'historyevents': historyevents,
    })


# @login_required()
# @cache_page(60 * 15)
def neighborhood(request):

    return render(request, 'neighborhood.html', {})


# @login_required()
# @cache_page(60 * 15)
def weather(request):

    return render(request, 'weather.html', {})


# @login_required()
# @cache_page(60 * 15)
def rules(request):
    categories = HelpCategory.objects.filter(active=True) #.order_by('cat_ordering')

    return render(request, 'rules.html', {
        'categories': categories,
    })

# @login_required()
# @cache_page(60 * 15)
def service(request):
    institutions = Institution.objects.select_related().filter(active=True).order_by('entity_type', 'name_global')

    return render(request, 'service.html', {
        'institutions': institutions,
    })


# @login_required()
# @cache_page(60 * 15)
def history(request):
    events = History.objects.filter(active=True)

    return render(request, 'history.html', {
        'events': events,
    })

# @login_required()
def news(request):

    return render(request, 'news.html', {})

@csrf_exempt
def messageImage(request):

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = MessageImageForm(request.POST, request.FILES)

        # print(form.files)
        # print(form.data)
        # print('++++++++++++++')

        results = []

        for img in form.files.getlist('files[]'):
            # print(img)

            nm = MessageImage()
            nm.name = img.name
            nm.image = img
            nm.save()



            results.append({'id': nm.id, 'name': nm.name})

            # print(nm)
        # print(nm.id)
    # print(results)

    jsresults = json.dumps({'results': results})

    return JsonResponse(dict(images=list(results)))




def messageView(request):

    formok = False

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = MessageForm(request.POST)
        # print(form.data)
        # check whether it's valid:



        if form.is_valid():
            # print(form.cleaned_data)

            formok = True

            nm = Message()

            nm.title = form.cleaned_data['title']
            nm.sender_name = form.cleaned_data['sender_name']
            nm.sender_email = form.cleaned_data['sender_email']
            nm.sender_phone = form.cleaned_data['sender_phone']
            nm.message = form.cleaned_data['message']
            nm.rating = form.cleaned_data['rating']
            nm.house = form.cleaned_data['house']
            # nm.picture = form.cleaned_data['picture']


            nm.save()

            for imgid in form.cleaned_data['messageimageattachements'].split(','):
                if imgid > '':
                    try:
                        img = MessageImage.objects.get(id=int(imgid))
                        nm.images.add(img)
                    except:
                        pass


            send_mail(
                '[SKILABOÐ AF VEF] %s' % form.cleaned_data['title'],
                'Sendandi %s\nSími: %s\n\n%s' % (form.cleaned_data['sender_name'], form.cleaned_data['sender_phone'], form.cleaned_data['message'] ),
                form.cleaned_data['sender_email'],
                [
                    'einar@nature.is',
                    'olfusborgir@olfusborgir.is',
                ],
                fail_silently=False,
            )

            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            # return HttpResponseRedirect('/thanks/')

            # if a GET (or any other method) we'll create a blank form
        else:
            form = MessageForm()
    else:
        form = MessageForm()

    return render(request, 'includes/messageform.html', {
        'messageform': form,
        'formok': formok,
    })



def webTechView(request):

    print('TechForm')
    print(request)

    formok = False


    if request.method == 'POST':
        print('POST')
        # create a form instance and populate it with data from the request:
        form = TechNoteForm(request.POST, request.FILES)
        print(form.data)
        print(form.files)
        # check whether it's valid:



        if form.is_valid():
            print(form.cleaned_data)

            formok = True

            nm = TechNote()

            nm.wt_subject = form.cleaned_data['wt_subject']
            nm.wt_name = form.cleaned_data['wt_name']
            nm.wt_email = form.cleaned_data['wt_email']
            nm.wt_note = form.cleaned_data['wt_note']
            # nm.picture = form.cleaned_data['picture']

            nm.wt_screenshot = form.files['wt_screenshot']

            nm.save()

            send_mail(
                '[TechNote AF VEF] %s' % form.cleaned_data['wt_subject'],
                'Sendandi %s\nSími: %s\n\n%s' % (form.cleaned_data['sender_name'], form.cleaned_data['sender_phone'], form.cleaned_data['message'] ),
                form.cleaned_data['sender_email'],
                [
                    'einar@nature.is',
                ],
                fail_silently=False,
            )

            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            # return HttpResponseRedirect('/thanks/')

            # if a GET (or any other method) we'll create a blank form
        else:
            form = TechNoteForm()
    else:
        form = TechNoteForm()



    return JsonResponse({"form-ok": 1})


def cookieaccept(request):

    resp = HttpResponse(request)
    resp.set_cookie('cookieaccept', value=True, max_age=None, expires=None, path='/', domain=None, secure=None,
                            httponly=False)

    return resp