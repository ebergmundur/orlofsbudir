import rest_framework
from django.contrib.gis.geos import Polygon
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from .models import HikePath, House, Institution, Image, History, EntityType, BaseCallTranslation, Person


class BaseCallTranslationSerializer(serializers.ModelSerializer):

    class Meta:
        model = BaseCallTranslation

        fields = (
            'base_master',
            'language',
            'name',
            'description',
            'details',
            # 'note',
        )




class PersonNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person


        fields = (
            'id',
            'name_global',
        )


class ImageSerilizer(GeoFeatureModelSerializer):
    gallery_display = serializers.CharField( )
    image_display = serializers.CharField( )
    photographer = PersonNameSerializer(many=False)
    basecalltranslation_set = BaseCallTranslationSerializer(many=True)

    class Meta:
        model = Image
        geo_field = 'image_location'

        fields = (
            'id',
            'name_global',
            'photographer',
            'photographed_date',
            'image_institution',
            'gallery_display',
            'image_display',
            'basecalltranslation_set',
        )



class EntityTypeSerializer(serializers.ModelSerializer):
    translation = BaseCallTranslationSerializer(many=True)

    class Meta:
        model = EntityType

        fields = (
            'id',
            'name_global',
            'icon',
            'translation',
        )

# class


class InstitutionSerializer(GeoFeatureModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    basecalltranslation_set = BaseCallTranslationSerializer( many=True )
    entity_type = serializers.CharField(source='entity_type.icon')
    logo_display = serializers.CharField()
    other_image = ImageSerilizer(many=True)

    class Meta:
        model = Institution
        geo_field = "location"
        auto_bbox = True

        # you can also explicitly declare which fields you want to include
        # as with a ModelSerializer.
        fields = (
            'id',
            'name_global',
            'logo',
            'street_address',
            'post_code',
            'phone',
            'email',
            'website',
            'entity_type',
            'logo_display',
            'basecalltranslation_set',
            'other_image',
            # 'translation',#
        )

class InstitutionShortSerializer(GeoFeatureModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    logo_display = serializers.CharField()

    class Meta:
        model = Institution
        geo_field = "location"

        # you can also explicitly declare which fields you want to include
        # as with a ModelSerializer.
        fields = (
            'id',
            'name_global',
            'logo_display',
            'phone',
            'email',
            'website',
        )


class HikePathSerializer(GeoFeatureModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    basecalltranslation_set = BaseCallTranslationSerializer(many=True)
    length = serializers.FloatField( source='path_length' )
    color = serializers.CharField(source='path_color')

    class Meta:
        model = HikePath
        geo_field = "path"
        auto_bbox = True

        # you can also explicitly declare which fields you want to include
        # as with a ModelSerializer.
        fields = (
            'id',
            'name_global',
            'main_image',
            'path_color',
            'color',
            'difficulty',
            'length',
            'basecalltranslation_set',
        )

        # def get_properties(self, instance, fields):
        #     # This is a PostgreSQL HStore field, which django maps to a dict
        #     return instance.metadata
        #
        # def unformat_geojson(self, feature):
        #     attrs = {
        #         self.Meta.geo_field: feature["geometry"],
        #         "metadata": feature["properties"]
        #     }
        #
        #     if self.Meta.bbox_geo_field and "bbox" in feature:
        #         attrs[self.Meta.bbox_geo_field] = Polygon.from_bbox(feature["bbox"])
        #
        #     return attrs


class HouseSerializer(GeoFeatureModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    basecalltranslation_set = BaseCallTranslationSerializer(many=True)
    # color = serializers.CharField( source='path_color' )
    images = ImageSerilizer(many=True)
    main_image = ImageSerilizer(many=False)
    owned_by = InstitutionShortSerializer(many=True)

    class Meta:
        model = House
        geo_field = "house_location"
        auto_bbox = True

        # you can also explicitly declare which fields you want to include
        # as with a ModelSerializer.
        fields = (
            'id',
            'name_global',
            'basecalltranslation_set',
            'number',
            'main_image',
            'images',
            'owned_by',
            'service_house',
            'special_needs',
        )

class HistorySerializer(GeoFeatureModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    basecalltranslation_set = BaseCallTranslationSerializer(many=True)
    images = ImageSerilizer(many=True)
    main_image = ImageSerilizer(many=False)
    # color = serializers.CharField( source='path_color' )

    class Meta:
        model = History
        geo_field = "history_location"

        # you can also explicitly declare which fields you want to include
        # as with a ModelSerializer.
        fields = (
            'id',
            'name_global',
            'basecalltranslation_set',
            'images',
            'period_from_date',
            'period_to_date',
            'actual_date',
            'people',
            'institutions',
            'main_image',
        )
