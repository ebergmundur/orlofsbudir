from django.utils.translation import ugettext_lazy as _
from jet.dashboard.dashboard import Dashboard, AppIndexDashboard
# from jet.dashboard.dashboard_modules import yandex_metrika
from jet.dashboard import modules
from jet.dashboard.dashboard_modules import google_analytics
from jet.dashboard.modules import DashboardModule


from .models import Message


class RecentMessage(DashboardModule):
    title = _('Skilaboð')
    title_url = '/admin/olfus/message/' #Message.get_admin_changelist_url()
    template = 'dashboard_messages.html'
    limit = 10

    def init_with_context(self, context):
        self.children = Message.objects.order_by('-created')[:self.limit]



class CustomIndexDashboard(Dashboard):
    columns = 2

    def init_with_context(self, context):
       # self.available_children.append(yandex_metrika.YandexMetrikaVisitorsTotals)
       # self.available_children.append(yandex_metrika.YandexMetrikaVisitorsChart)
       # self.available_children.append(yandex_metrika.YandexMetrikaPeriodVisitors)

       self.children.append(modules.RecentActions(
           _('Recent Actions'),
           10,
           column=0,
           order=0
       ))
       self.children.append(modules.AppList(
           _('Applications'),
           exclude=('auth.*',),
           column=0,
           order=0
       ))
       self.children.append(RecentMessage(
       #     _('Skilaboð'),
       #     10,
       #     column=0,
       #     order=0
       ))
       self.available_children.append(google_analytics.GoogleAnalyticsVisitorsTotals)
       self.available_children.append(google_analytics.GoogleAnalyticsVisitorsChart)
       self.available_children.append(google_analytics.GoogleAnalyticsPeriodVisitors)
