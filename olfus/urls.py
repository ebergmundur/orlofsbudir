from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.base, name='base'),
    url(r'^nagrenni/$', views.neighborhood, name='neighborhood'),
    url(r'^upplysingar/$', views.rules, name='rules'),
    url(r'^thjonusta/$', views.service, name='service'),
    url(r'^saga/$', views.history, name='history'),
    url(r'^adofinni/$', views.news, name='news'),
    url(r'^vedur/$', views.weather, name='weather'),

    url(r'hikepath/$', views.HikePathList.as_view()),
    url(r'^hikepath/(?P<pk>[0-9]+)/$', views.HikePathDetail.as_view()),
    url(r'^house/$', views.HouseList.as_view()),
    url(r'^house/(?P<pk>[0-9]+)/$', views.HouseDetail.as_view()),
    url(r'^institution/$', views.InstitutionList.as_view()),
    url(r'^institution/(?P<pk>[0-9]+)/$', views.InstitutionDetail.as_view()),
    url(r'^historyinstitution/$', views.HistoryList.as_view()),
    url(r'^history/(?P<pk>[0-9]+)/$', views.HistoryDetail.as_view()),
    url(r'^message/$', views.messageView ),
    url(r'^techform/$', views.webTechView ),
    url(r'^message_image/$', views.messageImage ),
    url(r'^cookieaccept/$', views.cookieaccept ),

]
