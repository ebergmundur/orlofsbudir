# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-16 18:49
from __future__ import unicode_literals

import colorfield.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('olfus', '0009_auto_20170214_1117'),
    ]

    operations = [
        migrations.AddField(
            model_name='hikepath',
            name='difficulty',
            field=models.SmallIntegerField(choices=[(1, 'Létt'), (2, 'Nokkuð létt'), (3, 'Miðlungs'), (4, 'Nokuð erfið'), (5, 'Erfið')], default=2),
        ),
        migrations.AddField(
            model_name='hikepath',
            name='path_color',
            field=colorfield.fields.ColorField(default='#FF0000', max_length=10),
        ),
        migrations.AlterField(
            model_name='basecalltranslation',
            name='language',
            field=models.CharField(choices=[('is', 'Íslenska'), ('en', 'Enska')], max_length=4),
        ),
    ]
