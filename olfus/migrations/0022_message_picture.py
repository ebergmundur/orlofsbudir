# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-11 10:42
from __future__ import unicode_literals

from django.db import migrations
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
       # ('filer', '0009_auto_20170201_1527'),
        ('olfus', '0021_auto_20170310_2123'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='picture',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='message_picture', to='filer.Image'),
        ),
    ]
