# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-10 11:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('olfus', '0018_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='history',
            name='show_full_date',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='created',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
