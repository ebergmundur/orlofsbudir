from django.utils.translation import ugettext_lazy as _
from datetime import timedelta
from django.contrib import admin
from django.contrib.sites.shortcuts import get_current_site
from adminsortable2.admin import SortableAdminMixin
from django.utils import timezone
from leaflet.admin import LeafletGeoAdminMixin
from leaflet.admin import LeafletGeoAdmin
from jet.filters import RelatedFieldAjaxListFilter
from jet.admin import CompactInline


from .models import BaseClass, BaseCallTranslation, EntityType, Help, Person, Institution, Image, Document, \
    Announcement, History, House, HikePath, HelpCategory, Settings, Teaser, Message

# , TranslatedCharField, TranslatedTextField
#
#
# class CharFieldInline(admin.StackedInline):
#     model = TranslatedCharField
#     extra = 1
#     # fk_name = 'baseclass_id'
#
#
# class TextFieldInline(admin.StackedInline):
#     model = TranslatedTextField
#     extra = 1
#     # fk_name = 'baseclass_id'
#
class BaseCallTranslationInline(CompactInline):
    model = BaseCallTranslation
    extra = 1
    # fk_name = 'baseclass_id'

#
# @admin.register(BaseClass)
class BaseClassAdmin(admin.ModelAdmin):
    # list_display = ('list_name',)
    #
    # fieldsets = (
    #     (None, {
    #         'classes': ('collapse open'),
    #         'fields': (
    #             'list_name',
    #             # 'sites',
    #             # 'note',
    #             # 'owner',
    #         )
    #     }),
    # ('Efni', {
    #          'classes': ('collapse open'),
    #         'fields': ()
    #      }),
    # )

    raw_id_fields = (
        'sites',
        'owner',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
        ],
        'm2m': [
            'sites',
        ],
    }

    inlines = [BaseCallTranslationInline]

    superuser = False

    def formfield_for_dbfield(self, db_field, **kwargs):
        request = kwargs['request']
        field = super(BaseClassAdmin, self).formfield_for_dbfield(db_field, **kwargs)

        if not 'change' in request.path:
            if db_field.name == 'owner':
                field.initial = request.user
                # if request.user.is_superuser == False:
                #     self.exclude.append('owner')
            elif db_field.name == 'sites':
                field.initial = [get_current_site(request)]
            elif db_field.name == 'active_date':
                field.initial = timezone.now()
            elif db_field.name == 'inactive_date':
                field.initial = timezone.now() + timedelta(days=2)

        return field

    class Media:
        css = {
            "all": ("olfus/adminfix.css",)
        }
        js = [
            'grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            'olfus/tinymce_setup.js',
            # 'olfus/grappelli.js',
            # 'jquery-lazy/jquery.lazy.js',
        ]


@admin.register(EntityType)
class EntityTypeAdmin(SortableAdminMixin, BaseClassAdmin):
    list_display = ('name_global', 'ordering', 'icon', 'admin_icon')
    # list_editable = ['ordering']
    #
    # readonly_fields = ('admin_icon')



@admin.register(Teaser)
class EntityTypeAdmin(SortableAdminMixin, BaseClassAdmin):
    list_display = ('name_global', 'ordering', 'icon', )
    # list_editable = ['ordering']
    #
    # readonly_fields = ('admin_icon')




@admin.register(HelpCategory)
class HelpCategoryAdmin(SortableAdminMixin, BaseClassAdmin):
    list_display = ('name_global', 'ordering')

    # list_editable = ['ordering',]


    raw_id_fields = (
        'sites',
        'owner',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
        ],
        'm2m': [
            'sites',
        ],
    }



@admin.register(Help)
class HelpAdmin(SortableAdminMixin, BaseClassAdmin, LeafletGeoAdmin):
    list_display = ('name_global', 'ordering')

    list_filter = (
        ('category', RelatedFieldAjaxListFilter),
    )


    raw_id_fields = (
        'sites',
        'owner',
        'category',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
        ],
        'm2m': [
            'sites',
            'category',
        ],
    }


@admin.register(Person)
class PersonAdmin(BaseClassAdmin):

    raw_id_fields = (
        'sites',
        'owner',
        'other_image',
        'other_document',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
        ],
        'm2m': [
            'sites',
            'owned_by',
            'other_image',
            'other_document',
        ],
    }


@admin.register(HikePath)
class HikePathAdmin(BaseClassAdmin, LeafletGeoAdmin):

    raw_id_fields = (
        'sites',
        'owner',
        'images',
        'main_image',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
            'main_image',
        ],
        'm2m': [
            'sites',
            'owned_by',
            'other_image',
        ],
    }


@admin.register(Institution)
class InstitutionAdmin(BaseClassAdmin, LeafletGeoAdmin):
    list_display = ('name_global', 'entity_type', 'active', 'on_service_list', 'logo_image', 'email', 'phone', 'website')


    list_filter = (
        ('entity_type', RelatedFieldAjaxListFilter),
        'active',
        'on_service_list',
    )


    list_editable = ['on_service_list',]

    raw_id_fields = (
        'sites',
        'owner',
        'other_image',
        'other_document',
        'entity_type',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
            'entity_type',
        ],
        'm2m': [
            'sites',
            'other_image',
            'other_document',
        ],
    }


@admin.register(House)
class HouseAdmin(BaseClassAdmin, LeafletGeoAdmin):
    list_display = ('name_global', 'number', 'owners')
    list_filter = ['owned_by', ]
    # save_as = True
    # raw_id_fields = (
    #     'sites',
    #     'owner',
    #     'owned_by',
    #     'main_image',
    #     'images',
    # )
    # # define the related_lookup_fields
    # autocomplete_lookup_fields = {
    #     'fk': [
    #         'owner',
    #         'main_image',
    #     ],
    #     'm2m': [
    #         'sites',
    #         'owned_by',
    #         'images',
    #     ],
    # }



@admin.register(Document)
class DocumentAdmin(BaseClassAdmin):
    pass


@admin.register(Image)
class ImageAdmin(BaseClassAdmin, LeafletGeoAdmin):
    list_display = ('admin_image', 'name_global', )
    raw_id_fields = (
        'sites',
        'owner',
        'image_institution',
        'photographer',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
            'image_institution',
            'photographer',
        ],
        'm2m': [
            'sites',
        ],
    }


@admin.register(Announcement)
class AnnouncementAdmin(BaseClassAdmin, LeafletGeoAdmin):
    list_display = ('name_global', 'active_date', 'inactive_date', 'active', 'in_action', 'alert_type' )
    date_hierarchy = 'active_date'
    raw_id_fields = (
        'sites',
        'owner',
        'houses',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
            'houses',
        ],
        'm2m': [
            'sites',
        ],
    }



@admin.register(History)
class HistoryAdmin(BaseClassAdmin, LeafletGeoAdmin):
    list_display = ('name_global', 'actual_date', )
    date_hierarchy = 'actual_date'

    raw_id_fields = (
        'sites',
        'owner',
        'main_image',
        'images',
        'people',
        'institutions',
        'documents',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'owner',
            'main_image',
        ],
        'm2m': [
            'sites',
            'images',
            'people',
            'institutions',
            'documents',
        ],
    }





@admin.register(Settings)
class SettingsAdmin(BaseClassAdmin):
    list_display = ('name_global', 'active', 'default',)

    # raw_id_fields = (
    #     'sites',
    #     'owner',
    #     'head_image',
    # )
    # # define the related_lookup_fields
    # autocomplete_lookup_fields = {
    #     'fk': [
    #         'owner',
    #     ],
    #     'm2m': [
    #         'sites',
    #         'head_image',
    #     ],
    # }



@admin.register(Message)
class SettingsAdmin(admin.ModelAdmin):
    list_display = ( 'title', 'house', 'sender_name', 'sender_email', 'sender_phone', 'created', 'is_done', 'rating' , 'image_count')

    raw_id_fields = (
        'house',
    )
    # define the related_lookup_fields
    autocomplete_lookup_fields = {
        'fk': [
            'house',
        ],
    }

    readonly_fields = ['images_list']

    exclude = ['picture']
