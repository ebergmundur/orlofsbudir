from django.contrib.gis.gdal import CoordTransform
from django.contrib.gis.gdal import SpatialReference
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models
# from django.db import models
from filer.models.abstract import BaseImage
from filer.fields.image import FilerImageField
from polymorphic.models import PolymorphicModel
from polymorphic.manager import PolymorphicManager
from polymorphic.query import PolymorphicQuerySet
from filer.fields.file import FilerFileField
from django.conf import settings
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.contrib.auth.models import User
from filer.models.filemodels import File
from colorfield.fields import ColorField
from easy_thumbnails.files import get_thumbnailer


IS93 = SpatialReference(3057)
WGS84 = SpatialReference(4326)
STADFG = SpatialReference(4659)
stfw = CoordTransform( STADFG, WGS84 )


ICONS = (
    ('aerialway', _('aerialway')),
    ('airfield', _('airfield')),
    ('airport', _('airport')),
    ('alcohol-shop', _('alcohol-shop')),
    ('america-football', _('america-football')),
    ('amusement-park', _('amusement-park')),
    ('aquarium', _('aquarium')),
    ('art-gallery', _('art-gallery')),
    ('attraction', _('attraction')),
    ('bakery', _('bakery')),
    ('bank', _('bank')),
    ('bar', _('bar')),
    ('barrier', _('barrier')),
    ('baseball', _('baseball')),
    ('basketball', _('basketball')),
    ('bbq', _('bbq')),
    ('beer', _('beer')),
    ('bicycle', _('bicycle')),
    ('bicycle-share', _('bicycle-share')),
    ('blood-bank', _('blood-bank')),
    ('buddhism', _('buddhism')),
    ('building', _('building')),
    ('building-alt1', _('building-alt1')),
    ('bus', _('bus')),
    ('cafe', _('cafe')),
    ('campsite', _('campsite')),
    ('car', _('car')),
    ('castle', _('castle')),
    ('cemetery', _('cemetery')),
    ('cinema', _('cinema')),
    ('circle', _('circle')),
    ('circle-stroked', _('circle-stroked')),
    ('city', _('city')),
    ('clothing-store', _('clothing-store')),
    ('college', _('college')),
    ('commercial', _('commercial')),
    ('cricket', _('cricket')),
    ('cross', _('cross')),
    ('dam', _('dam')),
    ('danger', _('danger')),
    ('defibrillator', _('defibrillator')),
    ('dentist', _('dentist')),
    ('doctor', _('doctor')),
    ('dog-park', _('dog-park')),
    ('drinking-water', _('drinking-water')),
    ('embassy', _('embassy')),
    ('emergency-phone', _('emergency-phone')),
    ('entrance', _('entrance')),
    ('entrance-alt1', _('entrance-alt1')),
    ('farm', _('farm')),
    ('fast-food', _('fast-food')),
    ('fence', _('fence')),
    ('ferry', _('ferry')),
    ('fire-station', _('fire-station')),
    ('florist', _('florist')),
    ('fuel', _('fuel')),
    ('gaming', _('gaming')),
    ('garden', _('garden')),
    ('garden-center', _('garden-center')),
    ('gift', _('gift')),
    ('golf', _('golf')),
    ('grocery', _('grocery')),
    ('hairdresser', _('hairdresser')),
    ('harbor', _('harbor')),
    ('heart', _('heart')),
    ('heliport', _('heliport')),
    ('home', _('home')),
    ('horse-riding', _('horse-riding')),
    ('hospital', _('hospital')),
    ('ice-cream', _('ice-cream')),
    ('industry', _('industry')),
    ('information', _('information')),
    ('karaoke', _('karaoke')),
    ('landmark', _('landmark')),
    ('landuse', _('landuse')),
    ('laundry', _('laundry')),
    ('library', _('library')),
    ('lighthouse', _('lighthouse')),
    ('lodging', _('lodging')),
    ('logging', _('logging')),
    ('marker', _('marker')),
    ('marker-stroked', _('marker-stroked')),
    ('mobile-phone', _('mobile-phone')),
    ('monument', _('monument')),
    ('mountain', _('mountain')),
    ('museum', _('museum')),
    ('music', _('music')),
    ('natural', _('natural')),
    ('park', _('park')),
    ('park-alt1', _('park-alt1')),
    ('parking', _('parking')),
    ('parking-garage', _('parking-garage')),
    ('pharmacy', _('pharmacy')),
    ('picnic-site', _('picnic-site')),
    ('pitch', _('pitch')),
    ('place-of-worship', _('place-of-worship')),
    ('playground', _('playground')),
    ('police', _('police')),
    ('post', _('post')),
    ('prison', _('prison')),
    ('rail', _('rail')),
    ('rail-light', _('rail-light')),
    ('rail-metro', _('rail-metro')),
    ('ranger-station', _('ranger-station')),
    ('recycling', _('recycling')),
    ('religious-christian', _('religious-christian')),
    ('religious-jewish', _('religious-jewish')),
    ('religious-muslim', _('religious-muslim')),
    ('residential-community', _('residential-community')),
    ('restaurant', _('restaurant')),
    ('roadblock', _('roadblock')),
    ('rocket', _('rocket')),
    ('school', _('school')),
    ('scooter', _('scooter')),
    ('shelter', _('shelter')),
    ('shop', _('shop')),
    ('skiing', _('skiing')),
    ('slaughterhouse', _('slaughterhouse')),
    ('snowmobile', _('snowmobile')),
    ('soccer', _('soccer')),
    ('square', _('square')),
    ('square-stroked', _('square-stroked')),
    ('stadium', _('stadium')),
    ('star', _('star')),
    ('star-stroked', _('star-stroked')),
    ('suitcase', _('suitcase')),
    ('sushi', _('sushi')),
    ('swimming', _('swimming')),
    ('teahouse', _('teahouse')),
    ('telephone', _('telephone')),
    ('tennis', _('tennis')),
    ('theatre', _('theatre')),
    ('toilet', _('toilet')),
    ('town', _('town')),
    ('town-hall', _('town-hall')),
    ('triangle', _('triangle')),
    ('triangle-stroked', _('triangle-stroked')),
    ('veterinary', _('veterinary')),
    ('village', _('village')),
    ('volcano', _('volcano')),
    ('warehouse', _('warehouse')),
    ('waste-basket', _('waste-basket')),
    ('water', _('water')),
    ('wetland', _('wetland')),
    ('wheelchair', _('wheelchair')),
    ('zoo', _('zoo')),

)

FAICONS = (

('fa-500px', mark_safe('<i class="fa fa-500px" aria-hidden="true"></i> fa-500px') ),
('fa-address-book', mark_safe('<i class="fa fa-address-book" aria-hidden="true"></i> fa-address-book') ),
('fa-address-book-o', mark_safe('<i class="fa fa-address-book-o" aria-hidden="true"></i> fa-address-book-o') ),
('fa-address-card', mark_safe('<i class="fa fa-address-card" aria-hidden="true"></i> fa-address-card') ),
('fa-address-card-o', mark_safe('<i class="fa fa-address-card-o" aria-hidden="true"></i> fa-address-card-o') ),
('fa-adjust', mark_safe('<i class="fa fa-adjust" aria-hidden="true"></i> fa-adjust') ),
('fa-adn', mark_safe('<i class="fa fa-adn" aria-hidden="true"></i> fa-adn') ),
('fa-align-center', mark_safe('<i class="fa fa-align-center" aria-hidden="true"></i> fa-align-center') ),
('fa-align-justify', mark_safe('<i class="fa fa-align-justify" aria-hidden="true"></i> fa-align-justify') ),
('fa-align-left', mark_safe('<i class="fa fa-align-left" aria-hidden="true"></i> fa-align-left') ),
('fa-align-right', mark_safe('<i class="fa fa-align-right" aria-hidden="true"></i> fa-align-right') ),
('fa-amazon', mark_safe('<i class="fa fa-amazon" aria-hidden="true"></i> fa-amazon') ),
('fa-ambulance', mark_safe('<i class="fa fa-ambulance" aria-hidden="true"></i> fa-ambulance') ),
('fa-american-sign-language-interpreting', mark_safe('<i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i> fa-american-sign-language-interpreting') ),
('ffa-anchor', mark_safe('<i class="fa ffa-anchor" aria-hidden="true"></i> ffa-anchor') ),
('ffa-android', mark_safe('<i class="fa ffa-android" aria-hidden="true"></i> ffa-android') ),
('fa-angellist', mark_safe('<i class="fa fa-angellist" aria-hidden="true"></i> fa-angellist') ),
('fa-angle-double-down', mark_safe('<i class="fa fa-angle-double-down" aria-hidden="true"></i> fa-angle-double-down') ),
('fa-angle-double-left', mark_safe('<i class="fa fa-angle-double-left" aria-hidden="true"></i> fa-angle-double-left') ),
('fa-angle-double-right', mark_safe('<i class="fa fa-angle-double-right" aria-hidden="true"></i> fa-angle-double-right') ),
('fa-angle-double-up', mark_safe('<i class="fa fa-angle-double-up" aria-hidden="true"></i> fa-angle-double-up') ),
('fa-angle-down', mark_safe('<i class="fa fa-angle-down" aria-hidden="true"></i> fa-angle-down') ),
('fa-angle-left', mark_safe('<i class="fa fa-angle-left" aria-hidden="true"></i> fa-angle-left') ),
('fa-angle-right', mark_safe('<i class="fa fa-angle-right" aria-hidden="true"></i> fa-angle-right') ),
('fa-angle-up', mark_safe('<i class="fa fa-angle-up" aria-hidden="true"></i> fa-angle-up') ),
('fa-apple', mark_safe('<i class="fa fa-apple" aria-hidden="true"></i> fa-apple') ),
('fa-archive', mark_safe('<i class="fa fa-archive" aria-hidden="true"></i> fa-archive') ),
('a-area-chart', mark_safe('<i class="fa a-area-chart" aria-hidden="true"></i> a-area-chart') ),
('fa-arrow-circle-down', mark_safe('<i class="fa fa-arrow-circle-down" aria-hidden="true"></i> fa-arrow-circle-down') ),
('fa-arrow-circle-left', mark_safe('<i class="fa fa-arrow-circle-left" aria-hidden="true"></i> fa-arrow-circle-left') ),
('fa-arrow-circle-o-down', mark_safe('<i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i> fa-arrow-circle-o-down') ),
('fa-arrow-circle-o-left', mark_safe('<i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> fa-arrow-circle-o-left') ),
('fa-arrow-circle-o-right', mark_safe('<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> fa-arrow-circle-o-right') ),
('fa-arrow-circle-o-up', mark_safe('<i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i> fa-arrow-circle-o-up') ),
('fa-arrow-circle-right', mark_safe('<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> fa-arrow-circle-right') ),
('fa-arrow-circle-up', mark_safe('<i class="fa fa-arrow-circle-up" aria-hidden="true"></i> fa-arrow-circle-up') ),
('fa-arrow-down', mark_safe('<i class="fa fa-arrow-down" aria-hidden="true"></i> fa-arrow-down') ),
('fa-arrow-left', mark_safe('<i class="fa fa-arrow-left" aria-hidden="true"></i> fa-arrow-left') ),
('fa-arrow-right', mark_safe('<i class="fa fa-arrow-right" aria-hidden="true"></i> fa-arrow-right') ),
('fa-arrow-up', mark_safe('<i class="fa fa-arrow-up" aria-hidden="true"></i> fa-arrow-up') ),
('fa-arrows', mark_safe('<i class="fa fa-arrows" aria-hidden="true"></i> fa-arrows') ),
('fa-arrows-alt', mark_safe('<i class="fa fa-arrows-alt" aria-hidden="true"></i> fa-arrows-alt') ),
('fa-arrows-h', mark_safe('<i class="fa fa-arrows-h" aria-hidden="true"></i> fa-arrows-h') ),
('fa-arrows-v', mark_safe('<i class="fa fa-arrows-v" aria-hidden="true"></i> fa-arrows-v') ),
('fa-asl-interpreting (alias)', mark_safe('<i class="fa fa-asl-interpreting (alias)" aria-hidden="true"></i> fa-asl-interpreting (alias)') ),
('fa-assistive-listening-systems', mark_safe('<i class="fa fa-assistive-listening-systems" aria-hidden="true"></i> fa-assistive-listening-systems') ),
('fa-asterisk', mark_safe('<i class="fa fa-asterisk" aria-hidden="true"></i> fa-asterisk') ),
('fa-at', mark_safe('<i class="fa fa-at" aria-hidden="true"></i> fa-at') ),
('fa-audio-description', mark_safe('<i class="fa fa-audio-description" aria-hidden="true"></i> fa-audio-description') ),
('fa-automobile (alias)', mark_safe('<i class="fa fa-automobile (alias)" aria-hidden="true"></i> fa-automobile (alias)') ),
('fa-backward', mark_safe('<i class="fa fa-backward" aria-hidden="true"></i> fa-backward') ),
('fa-balance-scale', mark_safe('<i class="fa fa-balance-scale" aria-hidden="true"></i> fa-balance-scale') ),
('fa-ban', mark_safe('<i class="fa fa-ban" aria-hidden="true"></i> fa-ban') ),
('fa-bandcamp', mark_safe('<i class="fa fa-bandcamp" aria-hidden="true"></i> fa-bandcamp') ),
('fa-bank (alias)', mark_safe('<i class="fa fa-bank (alias)" aria-hidden="true"></i> fa-bank (alias)') ),
('fa-bar-chart', mark_safe('<i class="fa fa-bar-chart" aria-hidden="true"></i> fa-bar-chart') ),
('fa-bar-chart-o (alias)', mark_safe('<i class="fa fa-bar-chart-o (alias)" aria-hidden="true"></i> fa-bar-chart-o (alias)') ),
('fa-barcode', mark_safe('<i class="fa fa-barcode" aria-hidden="true"></i> fa-barcode') ),
('fa-bars', mark_safe('<i class="fa fa-bars" aria-hidden="true"></i> fa-bars') ),
('fa-bath', mark_safe('<i class="fa fa-bath" aria-hidden="true"></i> fa-bath') ),
('fa-bathtub (alias)', mark_safe('<i class="fa fa-bathtub (alias)" aria-hidden="true"></i> fa-bathtub (alias)') ),
('fa-battery (alias)', mark_safe('<i class="fa fa-battery (alias)" aria-hidden="true"></i> fa-battery (alias)') ),
('fa-battery-0 (alias)', mark_safe('<i class="fa fa-battery-0 (alias)" aria-hidden="true"></i> fa-battery-0 (alias)') ),
('fa-battery-1 (alias)', mark_safe('<i class="fa fa-battery-1 (alias)" aria-hidden="true"></i> fa-battery-1 (alias)') ),
('fa-battery-2 (alias)', mark_safe('<i class="fa fa-battery-2 (alias)" aria-hidden="true"></i> fa-battery-2 (alias)') ),
('fa-battery-3 (alias)', mark_safe('<i class="fa fa-battery-3 (alias)" aria-hidden="true"></i> fa-battery-3 (alias)') ),
('fa-battery-4 (alias)', mark_safe('<i class="fa fa-battery-4 (alias)" aria-hidden="true"></i> fa-battery-4 (alias)') ),
('fa-battery-empty', mark_safe('<i class="fa fa-battery-empty" aria-hidden="true"></i> fa-battery-empty') ),
('fa-battery-full', mark_safe('<i class="fa fa-battery-full" aria-hidden="true"></i> fa-battery-full') ),
('fa-battery-half', mark_safe('<i class="fa fa-battery-half" aria-hidden="true"></i> fa-battery-half') ),
('fa-battery-quarter', mark_safe('<i class="fa fa-battery-quarter" aria-hidden="true"></i> fa-battery-quarter') ),
('fa-battery-three-quarters', mark_safe('<i class="fa fa-battery-three-quarters" aria-hidden="true"></i> fa-battery-three-quarters') ),
('fa-bed', mark_safe('<i class="fa fa-bed" aria-hidden="true"></i> fa-bed') ),
('fa-beer', mark_safe('<i class="fa fa-beer" aria-hidden="true"></i> fa-beer') ),
('fa-behance', mark_safe('<i class="fa fa-behance" aria-hidden="true"></i> fa-behance') ),
('fa-behance-square', mark_safe('<i class="fa fa-behance-square" aria-hidden="true"></i> fa-behance-square') ),
('fa-bell', mark_safe('<i class="fa fa-bell" aria-hidden="true"></i> fa-bell') ),
('fa-bell-o', mark_safe('<i class="fa fa-bell-o" aria-hidden="true"></i> fa-bell-o') ),
('fa-bell-slash', mark_safe('<i class="fa fa-bell-slash" aria-hidden="true"></i> fa-bell-slash') ),
('fa-bell-slash-o', mark_safe('<i class="fa fa-bell-slash-o" aria-hidden="true"></i> fa-bell-slash-o') ),
('fa-bicycle', mark_safe('<i class="fa fa-bicycle" aria-hidden="true"></i> fa-bicycle') ),
('fa-binoculars', mark_safe('<i class="fa fa-binoculars" aria-hidden="true"></i> fa-binoculars') ),
('fa-birthday-cake', mark_safe('<i class="fa fa-birthday-cake" aria-hidden="true"></i> fa-birthday-cake') ),
('fa-bitbucket', mark_safe('<i class="fa fa-bitbucket" aria-hidden="true"></i> fa-bitbucket') ),
('fa-bitbucket-square', mark_safe('<i class="fa fa-bitbucket-square" aria-hidden="true"></i> fa-bitbucket-square') ),
('fa-bitcoin (alias)', mark_safe('<i class="fa fa-bitcoin (alias)" aria-hidden="true"></i> fa-bitcoin (alias)') ),
('fa-black-tie', mark_safe('<i class="fa fa-black-tie" aria-hidden="true"></i> fa-black-tie') ),
('fa-blind', mark_safe('<i class="fa fa-blind" aria-hidden="true"></i> fa-blind') ),
('fa-bluetooth', mark_safe('<i class="fa fa-bluetooth" aria-hidden="true"></i> fa-bluetooth') ),
('fa-bluetooth-b', mark_safe('<i class="fa fa-bluetooth-b" aria-hidden="true"></i> fa-bluetooth-b') ),
('fa-bold', mark_safe('<i class="fa fa-bold" aria-hidden="true"></i> fa-bold') ),
('fa-bolt', mark_safe('<i class="fa fa-bolt" aria-hidden="true"></i> fa-bolt') ),
('fa-bomb', mark_safe('<i class="fa fa-bomb" aria-hidden="true"></i> fa-bomb') ),
('fa-book', mark_safe('<i class="fa fa-book" aria-hidden="true"></i> fa-book') ),
('fa-bookmark', mark_safe('<i class="fa fa-bookmark" aria-hidden="true"></i> fa-bookmark') ),
('fa-bookmark-o', mark_safe('<i class="fa fa-bookmark-o" aria-hidden="true"></i> fa-bookmark-o') ),
('fa-braille', mark_safe('<i class="fa fa-braille" aria-hidden="true"></i> fa-braille') ),
('fa-briefcase', mark_safe('<i class="fa fa-briefcase" aria-hidden="true"></i> fa-briefcase') ),
('fa-btc', mark_safe('<i class="fa fa-btc" aria-hidden="true"></i> fa-btc') ),
('fa-bug', mark_safe('<i class="fa fa-bug" aria-hidden="true"></i> fa-bug') ),
('fa-building', mark_safe('<i class="fa fa-building" aria-hidden="true"></i> fa-building') ),
('fa-building-o', mark_safe('<i class="fa fa-building-o" aria-hidden="true"></i> fa-building-o') ),
('fa-bullhorn', mark_safe('<i class="fa fa-bullhorn" aria-hidden="true"></i> fa-bullhorn') ),
('fa-bullseye', mark_safe('<i class="fa fa-bullseye" aria-hidden="true"></i> fa-bullseye') ),
('fa-bus', mark_safe('<i class="fa fa-bus" aria-hidden="true"></i> fa-bus') ),
('fa-buysellads', mark_safe('<i class="fa fa-buysellads" aria-hidden="true"></i> fa-buysellads') ),
('fa-cab (alias)', mark_safe('<i class="fa fa-cab (alias)" aria-hidden="true"></i> fa-cab (alias)') ),
('fa-calculator', mark_safe('<i class="fa fa-calculator" aria-hidden="true"></i> fa-calculator') ),
('fa-calendar', mark_safe('<i class="fa fa-calendar" aria-hidden="true"></i> fa-calendar') ),
('fa-calendar-check-o', mark_safe('<i class="fa fa-calendar-check-o" aria-hidden="true"></i> fa-calendar-check-o') ),
('fa-calendar-minus-o', mark_safe('<i class="fa fa-calendar-minus-o" aria-hidden="true"></i> fa-calendar-minus-o') ),
('fa-calendar-o', mark_safe('<i class="fa fa-calendar-o" aria-hidden="true"></i> fa-calendar-o') ),
('fa-calendar-plus-o', mark_safe('<i class="fa fa-calendar-plus-o" aria-hidden="true"></i> fa-calendar-plus-o') ),
('fa-calendar-times-o', mark_safe('<i class="fa fa-calendar-times-o" aria-hidden="true"></i> fa-calendar-times-o') ),
('fa-camera', mark_safe('<i class="fa fa-camera" aria-hidden="true"></i> fa-camera') ),
('fa-camera-retro', mark_safe('<i class="fa fa-camera-retro" aria-hidden="true"></i> fa-camera-retro') ),
('fa-car', mark_safe('<i class="fa fa-car" aria-hidden="true"></i> fa-car') ),
('fa-caret-down', mark_safe('<i class="fa fa-caret-down" aria-hidden="true"></i> fa-caret-down') ),
('fa-caret-left', mark_safe('<i class="fa fa-caret-left" aria-hidden="true"></i> fa-caret-left') ),
('fa-caret-right', mark_safe('<i class="fa fa-caret-right" aria-hidden="true"></i> fa-caret-right') ),
('fa-caret-square-o-down', mark_safe('<i class="fa fa-caret-square-o-down" aria-hidden="true"></i> fa-caret-square-o-down') ),
('fa-caret-square-o-left', mark_safe('<i class="fa fa-caret-square-o-left" aria-hidden="true"></i> fa-caret-square-o-left') ),
('fa-caret-square-o-right', mark_safe('<i class="fa fa-caret-square-o-right" aria-hidden="true"></i> fa-caret-square-o-right') ),
('fa-caret-square-o-up', mark_safe('<i class="fa fa-caret-square-o-up" aria-hidden="true"></i> fa-caret-square-o-up') ),
('fa-caret-up', mark_safe('<i class="fa fa-caret-up" aria-hidden="true"></i> fa-caret-up') ),
('fa-cart-arrow-down', mark_safe('<i class="fa fa-cart-arrow-down" aria-hidden="true"></i> fa-cart-arrow-down') ),
('fa-cart-plus', mark_safe('<i class="fa fa-cart-plus" aria-hidden="true"></i> fa-cart-plus') ),
('fa-cc', mark_safe('<i class="fa fa-cc" aria-hidden="true"></i> fa-cc') ),
('fa-cc-amex', mark_safe('<i class="fa fa-cc-amex" aria-hidden="true"></i> fa-cc-amex') ),
('fa-cc-diners-club', mark_safe('<i class="fa fa-cc-diners-club" aria-hidden="true"></i> fa-cc-diners-club') ),
('fa-cc-discover', mark_safe('<i class="fa fa-cc-discover" aria-hidden="true"></i> fa-cc-discover') ),
('fa-cc-jcb', mark_safe('<i class="fa fa-cc-jcb" aria-hidden="true"></i> fa-cc-jcb') ),
('fa-cc-mastercard', mark_safe('<i class="fa fa-cc-mastercard" aria-hidden="true"></i> fa-cc-mastercard') ),
('fa-cc-paypal', mark_safe('<i class="fa fa-cc-paypal" aria-hidden="true"></i> fa-cc-paypal') ),
('fa-cc-stripe', mark_safe('<i class="fa fa-cc-stripe" aria-hidden="true"></i> fa-cc-stripe') ),
('fa-cc-visa', mark_safe('<i class="fa fa-cc-visa" aria-hidden="true"></i> fa-cc-visa') ),
('fa-certificate', mark_safe('<i class="fa fa-certificate" aria-hidden="true"></i> fa-certificate') ),
('fa-chain (alias)', mark_safe('<i class="fa fa-chain (alias)" aria-hidden="true"></i> fa-chain (alias)') ),
('fa-chain-broken', mark_safe('<i class="fa fa-chain-broken" aria-hidden="true"></i> fa-chain-broken') ),
('fa-check', mark_safe('<i class="fa fa-check" aria-hidden="true"></i> fa-check') ),
('fa-check-circle', mark_safe('<i class="fa fa-check-circle" aria-hidden="true"></i> fa-check-circle') ),
('fa-check-circle-o', mark_safe('<i class="fa fa-check-circle-o" aria-hidden="true"></i> fa-check-circle-o') ),
('fa-check-square', mark_safe('<i class="fa fa-check-square" aria-hidden="true"></i> fa-check-square') ),
('fa-check-square-o', mark_safe('<i class="fa fa-check-square-o" aria-hidden="true"></i> fa-check-square-o') ),
('fa-chevron-circle-down', mark_safe('<i class="fa fa-chevron-circle-down" aria-hidden="true"></i> fa-chevron-circle-down') ),
('fa-chevron-circle-left', mark_safe('<i class="fa fa-chevron-circle-left" aria-hidden="true"></i> fa-chevron-circle-left') ),
('fa-chevron-circle-right', mark_safe('<i class="fa fa-chevron-circle-right" aria-hidden="true"></i> fa-chevron-circle-right') ),
('fa-chevron-circle-up', mark_safe('<i class="fa fa-chevron-circle-up" aria-hidden="true"></i> fa-chevron-circle-up') ),
('fa-chevron-down', mark_safe('<i class="fa fa-chevron-down" aria-hidden="true"></i> fa-chevron-down') ),
('fa-chevron-left', mark_safe('<i class="fa fa-chevron-left" aria-hidden="true"></i> fa-chevron-left') ),
('fa-chevron-right', mark_safe('<i class="fa fa-chevron-right" aria-hidden="true"></i> fa-chevron-right') ),
('fa-chevron-up', mark_safe('<i class="fa fa-chevron-up" aria-hidden="true"></i> fa-chevron-up') ),
('fa-child', mark_safe('<i class="fa fa-child" aria-hidden="true"></i> fa-child') ),
('fa-chrome', mark_safe('<i class="fa fa-chrome" aria-hidden="true"></i> fa-chrome') ),
('fa-circle', mark_safe('<i class="fa fa-circle" aria-hidden="true"></i> fa-circle') ),
('fa-circle-o', mark_safe('<i class="fa fa-circle-o" aria-hidden="true"></i> fa-circle-o') ),
('fa-circle-o-notch', mark_safe('<i class="fa fa-circle-o-notch" aria-hidden="true"></i> fa-circle-o-notch') ),
('fa-circle-thin', mark_safe('<i class="fa fa-circle-thin" aria-hidden="true"></i> fa-circle-thin') ),
('fa-clipboard', mark_safe('<i class="fa fa-clipboard" aria-hidden="true"></i> fa-clipboard') ),
('fa-clock-o', mark_safe('<i class="fa fa-clock-o" aria-hidden="true"></i> fa-clock-o') ),
('fa-clone', mark_safe('<i class="fa fa-clone" aria-hidden="true"></i> fa-clone') ),
('fa-close (alias)', mark_safe('<i class="fa fa-close (alias)" aria-hidden="true"></i> fa-close (alias)') ),
('fa-cloud', mark_safe('<i class="fa fa-cloud" aria-hidden="true"></i> fa-cloud') ),
('fa-cloud-download', mark_safe('<i class="fa fa-cloud-download" aria-hidden="true"></i> fa-cloud-download') ),
('fa-cloud-upload', mark_safe('<i class="fa fa-cloud-upload" aria-hidden="true"></i> fa-cloud-upload') ),
('fa-cny (alias)', mark_safe('<i class="fa fa-cny (alias)" aria-hidden="true"></i> fa-cny (alias)') ),
('fa-code', mark_safe('<i class="fa fa-code" aria-hidden="true"></i> fa-code') ),
('fa-code-fork', mark_safe('<i class="fa fa-code-fork" aria-hidden="true"></i> fa-code-fork') ),
('fa-codepen', mark_safe('<i class="fa fa-codepen" aria-hidden="true"></i> fa-codepen') ),
('fa-codiepie', mark_safe('<i class="fa fa-codiepie" aria-hidden="true"></i> fa-codiepie') ),
('fa-coffee', mark_safe('<i class="fa fa-coffee" aria-hidden="true"></i> fa-coffee') ),
('fa-cog', mark_safe('<i class="fa fa-cog" aria-hidden="true"></i> fa-cog') ),
('fa-cogs', mark_safe('<i class="fa fa-cogs" aria-hidden="true"></i> fa-cogs') ),
('fa-columns', mark_safe('<i class="fa fa-columns" aria-hidden="true"></i> fa-columns') ),
('fa-comment', mark_safe('<i class="fa fa-comment" aria-hidden="true"></i> fa-comment') ),
('fa-comment-o', mark_safe('<i class="fa fa-comment-o" aria-hidden="true"></i> fa-comment-o') ),
('fa-commenting', mark_safe('<i class="fa fa-commenting" aria-hidden="true"></i> fa-commenting') ),
('fa-commenting-o', mark_safe('<i class="fa fa-commenting-o" aria-hidden="true"></i> fa-commenting-o') ),
('fa-comments', mark_safe('<i class="fa fa-comments" aria-hidden="true"></i> fa-comments') ),
('fa-comments-o', mark_safe('<i class="fa fa-comments-o" aria-hidden="true"></i> fa-comments-o') ),
('fa-compass', mark_safe('<i class="fa fa-compass" aria-hidden="true"></i> fa-compass') ),
('fa-compress', mark_safe('<i class="fa fa-compress" aria-hidden="true"></i> fa-compress') ),
('fa-connectdevelop', mark_safe('<i class="fa fa-connectdevelop" aria-hidden="true"></i> fa-connectdevelop') ),
('fa-contao', mark_safe('<i class="fa fa-contao" aria-hidden="true"></i> fa-contao') ),
('fa-copy (alias)', mark_safe('<i class="fa fa-copy (alias)" aria-hidden="true"></i> fa-copy (alias)') ),
('fa-copyright', mark_safe('<i class="fa fa-copyright" aria-hidden="true"></i> fa-copyright') ),
('fa-creative-commons', mark_safe('<i class="fa fa-creative-commons" aria-hidden="true"></i> fa-creative-commons') ),
('fa-credit-card', mark_safe('<i class="fa fa-credit-card" aria-hidden="true"></i> fa-credit-card') ),
('fa-credit-card-alt', mark_safe('<i class="fa fa-credit-card-alt" aria-hidden="true"></i> fa-credit-card-alt') ),
('fa-crop', mark_safe('<i class="fa fa-crop" aria-hidden="true"></i> fa-crop') ),
('fa-crosshairs', mark_safe('<i class="fa fa-crosshairs" aria-hidden="true"></i> fa-crosshairs') ),
('fa-css3', mark_safe('<i class="fa fa-css3" aria-hidden="true"></i> fa-css3') ),
('fa-cube', mark_safe('<i class="fa fa-cube" aria-hidden="true"></i> fa-cube') ),
('fa-cubes', mark_safe('<i class="fa fa-cubes" aria-hidden="true"></i> fa-cubes') ),
('fa-cut (alias)', mark_safe('<i class="fa fa-cut (alias)" aria-hidden="true"></i> fa-cut (alias)') ),
('fa-cutlery', mark_safe('<i class="fa fa-cutlery" aria-hidden="true"></i> fa-cutlery') ),
('fa-dashboard (alias)', mark_safe('<i class="fa fa-dashboard (alias)" aria-hidden="true"></i> fa-dashboard (alias)') ),
('fa-dashcube', mark_safe('<i class="fa fa-dashcube" aria-hidden="true"></i> fa-dashcube') ),
('fa-database', mark_safe('<i class="fa fa-database" aria-hidden="true"></i> fa-database') ),
('fa-deaf', mark_safe('<i class="fa fa-deaf" aria-hidden="true"></i> fa-deaf') ),
('fa-deafness (alias)', mark_safe('<i class="fa fa-deafness (alias)" aria-hidden="true"></i> fa-deafness (alias)') ),
('fa-dedent (alias)', mark_safe('<i class="fa fa-dedent (alias)" aria-hidden="true"></i> fa-dedent (alias)') ),
('fa-delicious', mark_safe('<i class="fa fa-delicious" aria-hidden="true"></i> fa-delicious') ),
('fa-desktop', mark_safe('<i class="fa fa-desktop" aria-hidden="true"></i> fa-desktop') ),
('fa-deviantart', mark_safe('<i class="fa fa-deviantart" aria-hidden="true"></i> fa-deviantart') ),
('fa-diamond', mark_safe('<i class="fa fa-diamond" aria-hidden="true"></i> fa-diamond') ),
('fa-digg', mark_safe('<i class="fa fa-digg" aria-hidden="true"></i> fa-digg') ),
('fa-dollar (alias)', mark_safe('<i class="fa fa-dollar (alias)" aria-hidden="true"></i> fa-dollar (alias)') ),
('fa-dot-circle-o', mark_safe('<i class="fa fa-dot-circle-o" aria-hidden="true"></i> fa-dot-circle-o') ),
('fa-download', mark_safe('<i class="fa fa-download" aria-hidden="true"></i> fa-download') ),
('fa-dribbble', mark_safe('<i class="fa fa-dribbble" aria-hidden="true"></i> fa-dribbble') ),
('fa-drivers-license (alias)', mark_safe('<i class="fa fa-drivers-license (alias)" aria-hidden="true"></i> fa-drivers-license (alias)') ),
('fa-drivers-license-o (alias)', mark_safe('<i class="fa fa-drivers-license-o (alias)" aria-hidden="true"></i> fa-drivers-license-o (alias)') ),
('fa-dropbox', mark_safe('<i class="fa fa-dropbox" aria-hidden="true"></i> fa-dropbox') ),
('fa-drupal', mark_safe('<i class="fa fa-drupal" aria-hidden="true"></i> fa-drupal') ),
('fa-edge', mark_safe('<i class="fa fa-edge" aria-hidden="true"></i> fa-edge') ),
('fa-edit (alias)', mark_safe('<i class="fa fa-edit (alias)" aria-hidden="true"></i> fa-edit (alias)') ),
('fa-eercast', mark_safe('<i class="fa fa-eercast" aria-hidden="true"></i> fa-eercast') ),
('fa-eject', mark_safe('<i class="fa fa-eject" aria-hidden="true"></i> fa-eject') ),
('fa-ellipsis-h', mark_safe('<i class="fa fa-ellipsis-h" aria-hidden="true"></i> fa-ellipsis-h') ),
('fa-ellipsis-v', mark_safe('<i class="fa fa-ellipsis-v" aria-hidden="true"></i> fa-ellipsis-v') ),
('fa-empire', mark_safe('<i class="fa fa-empire" aria-hidden="true"></i> fa-empire') ),
('fa-envelope', mark_safe('<i class="fa fa-envelope" aria-hidden="true"></i> fa-envelope') ),
('fa-envelope-o', mark_safe('<i class="fa fa-envelope-o" aria-hidden="true"></i> fa-envelope-o') ),
('fa-envelope-open', mark_safe('<i class="fa fa-envelope-open" aria-hidden="true"></i> fa-envelope-open') ),
('fa-envelope-open-o', mark_safe('<i class="fa fa-envelope-open-o" aria-hidden="true"></i> fa-envelope-open-o') ),
('fa-envelope-square', mark_safe('<i class="fa fa-envelope-square" aria-hidden="true"></i> fa-envelope-square') ),
('fa-envira', mark_safe('<i class="fa fa-envira" aria-hidden="true"></i> fa-envira') ),
('fa-eraser', mark_safe('<i class="fa fa-eraser" aria-hidden="true"></i> fa-eraser') ),
('fa-etsy', mark_safe('<i class="fa fa-etsy" aria-hidden="true"></i> fa-etsy') ),
('fa-eur', mark_safe('<i class="fa fa-eur" aria-hidden="true"></i> fa-eur') ),
('fa-euro (alias)', mark_safe('<i class="fa fa-euro (alias)" aria-hidden="true"></i> fa-euro (alias)') ),
('fa-exchange', mark_safe('<i class="fa fa-exchange" aria-hidden="true"></i> fa-exchange') ),
('fa-exclamation', mark_safe('<i class="fa fa-exclamation" aria-hidden="true"></i> fa-exclamation') ),
('fa-exclamation-circle', mark_safe('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> fa-exclamation-circle') ),
('fa-exclamation-triangle', mark_safe('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> fa-exclamation-triangle') ),
('fa-expand', mark_safe('<i class="fa fa-expand" aria-hidden="true"></i> fa-expand') ),
('fa-expeditedssl', mark_safe('<i class="fa fa-expeditedssl" aria-hidden="true"></i> fa-expeditedssl') ),
('fa-external-link', mark_safe('<i class="fa fa-external-link" aria-hidden="true"></i> fa-external-link') ),
('fa-external-link-square', mark_safe('<i class="fa fa-external-link-square" aria-hidden="true"></i> fa-external-link-square') ),
('fa-eye', mark_safe('<i class="fa fa-eye" aria-hidden="true"></i> fa-eye') ),
('fa-eye-slash', mark_safe('<i class="fa fa-eye-slash" aria-hidden="true"></i> fa-eye-slash') ),
('fa-eyedropper', mark_safe('<i class="fa fa-eyedropper" aria-hidden="true"></i> fa-eyedropper') ),
('fa-fa (alias)', mark_safe('<i class="fa fa-fa (alias)" aria-hidden="true"></i> fa-fa (alias)') ),
('fa-facebook', mark_safe('<i class="fa fa-facebook" aria-hidden="true"></i> fa-facebook') ),
('fa-facebook-f (alias)', mark_safe('<i class="fa fa-facebook-f (alias)" aria-hidden="true"></i> fa-facebook-f (alias)') ),
('fa-facebook-official', mark_safe('<i class="fa fa-facebook-official" aria-hidden="true"></i> fa-facebook-official') ),
('fa-facebook-square', mark_safe('<i class="fa fa-facebook-square" aria-hidden="true"></i> fa-facebook-square') ),
('fa-fast-backward', mark_safe('<i class="fa fa-fast-backward" aria-hidden="true"></i> fa-fast-backward') ),
('fa-fast-forward', mark_safe('<i class="fa fa-fast-forward" aria-hidden="true"></i> fa-fast-forward') ),
('fa-fax', mark_safe('<i class="fa fa-fax" aria-hidden="true"></i> fa-fax') ),
('fa-feed (alias)', mark_safe('<i class="fa fa-feed (alias)" aria-hidden="true"></i> fa-feed (alias)') ),
('fa-female', mark_safe('<i class="fa fa-female" aria-hidden="true"></i> fa-female') ),
('fa-fighter-jet', mark_safe('<i class="fa fa-fighter-jet" aria-hidden="true"></i> fa-fighter-jet') ),
('fa-file', mark_safe('<i class="fa fa-file" aria-hidden="true"></i> fa-file') ),
('fa-file-archive-o', mark_safe('<i class="fa fa-file-archive-o" aria-hidden="true"></i> fa-file-archive-o') ),
('fa-file-audio-o', mark_safe('<i class="fa fa-file-audio-o" aria-hidden="true"></i> fa-file-audio-o') ),
('fa-file-code-o', mark_safe('<i class="fa fa-file-code-o" aria-hidden="true"></i> fa-file-code-o') ),
('fa-file-excel-o', mark_safe('<i class="fa fa-file-excel-o" aria-hidden="true"></i> fa-file-excel-o') ),
('fa-file-image-o', mark_safe('<i class="fa fa-file-image-o" aria-hidden="true"></i> fa-file-image-o') ),
('fa-file-movie-o (alias)', mark_safe('<i class="fa fa-file-movie-o (alias)" aria-hidden="true"></i> fa-file-movie-o (alias)') ),
('fa-file-o', mark_safe('<i class="fa fa-file-o" aria-hidden="true"></i> fa-file-o') ),
('fa-file-pdf-o', mark_safe('<i class="fa fa-file-pdf-o" aria-hidden="true"></i> fa-file-pdf-o') ),
('fa-file-photo-o (alias)', mark_safe('<i class="fa fa-file-photo-o (alias)" aria-hidden="true"></i> fa-file-photo-o (alias)') ),
('fa-file-picture-o (alias)', mark_safe('<i class="fa fa-file-picture-o (alias)" aria-hidden="true"></i> fa-file-picture-o (alias)') ),
('fa-file-powerpoint-o', mark_safe('<i class="fa fa-file-powerpoint-o" aria-hidden="true"></i> fa-file-powerpoint-o') ),
('fa-file-sound-o (alias)', mark_safe('<i class="fa fa-file-sound-o (alias)" aria-hidden="true"></i> fa-file-sound-o (alias)') ),
('fa-file-text', mark_safe('<i class="fa fa-file-text" aria-hidden="true"></i> fa-file-text') ),
('fa-file-text-o', mark_safe('<i class="fa fa-file-text-o" aria-hidden="true"></i> fa-file-text-o') ),
('fa-file-video-o', mark_safe('<i class="fa fa-file-video-o" aria-hidden="true"></i> fa-file-video-o') ),
('fa-file-word-o', mark_safe('<i class="fa fa-file-word-o" aria-hidden="true"></i> fa-file-word-o') ),
('fa-file-zip-o (alias)', mark_safe('<i class="fa fa-file-zip-o (alias)" aria-hidden="true"></i> fa-file-zip-o (alias)') ),
('fa-files-o', mark_safe('<i class="fa fa-files-o" aria-hidden="true"></i> fa-files-o') ),
('fa-film', mark_safe('<i class="fa fa-film" aria-hidden="true"></i> fa-film') ),
('fa-filter', mark_safe('<i class="fa fa-filter" aria-hidden="true"></i> fa-filter') ),
('fa-fire', mark_safe('<i class="fa fa-fire" aria-hidden="true"></i> fa-fire') ),
('fa-fire-extinguisher', mark_safe('<i class="fa fa-fire-extinguisher" aria-hidden="true"></i> fa-fire-extinguisher') ),
('fa-firefox', mark_safe('<i class="fa fa-firefox" aria-hidden="true"></i> fa-firefox') ),
('fa-first-order', mark_safe('<i class="fa fa-first-order" aria-hidden="true"></i> fa-first-order') ),
('fa-flag', mark_safe('<i class="fa fa-flag" aria-hidden="true"></i> fa-flag') ),
('fa-flag-checkered', mark_safe('<i class="fa fa-flag-checkered" aria-hidden="true"></i> fa-flag-checkered') ),
('fa-flag-o', mark_safe('<i class="fa fa-flag-o" aria-hidden="true"></i> fa-flag-o') ),
('fa-flash (alias)', mark_safe('<i class="fa fa-flash (alias)" aria-hidden="true"></i> fa-flash (alias)') ),
('fa-flask', mark_safe('<i class="fa fa-flask" aria-hidden="true"></i> fa-flask') ),
('fa-flickr', mark_safe('<i class="fa fa-flickr" aria-hidden="true"></i> fa-flickr') ),
('fa-floppy-o', mark_safe('<i class="fa fa-floppy-o" aria-hidden="true"></i> fa-floppy-o') ),
('fa-folder', mark_safe('<i class="fa fa-folder" aria-hidden="true"></i> fa-folder') ),
('fa-folder-o', mark_safe('<i class="fa fa-folder-o" aria-hidden="true"></i> fa-folder-o') ),
('fa-folder-open', mark_safe('<i class="fa fa-folder-open" aria-hidden="true"></i> fa-folder-open') ),
('fa-folder-open-o', mark_safe('<i class="fa fa-folder-open-o" aria-hidden="true"></i> fa-folder-open-o') ),
('fa-font', mark_safe('<i class="fa fa-font" aria-hidden="true"></i> fa-font') ),
('fa-font-awesome', mark_safe('<i class="fa fa-font-awesome" aria-hidden="true"></i> fa-font-awesome') ),
('fa-fonticons', mark_safe('<i class="fa fa-fonticons" aria-hidden="true"></i> fa-fonticons') ),
('fa-fort-awesome', mark_safe('<i class="fa fa-fort-awesome" aria-hidden="true"></i> fa-fort-awesome') ),
('fa-forumbee', mark_safe('<i class="fa fa-forumbee" aria-hidden="true"></i> fa-forumbee') ),
('fa-forward', mark_safe('<i class="fa fa-forward" aria-hidden="true"></i> fa-forward') ),
('fa-foursquare', mark_safe('<i class="fa fa-foursquare" aria-hidden="true"></i> fa-foursquare') ),
('fa-free-code-camp', mark_safe('<i class="fa fa-free-code-camp" aria-hidden="true"></i> fa-free-code-camp') ),
('fa-frown-o', mark_safe('<i class="fa fa-frown-o" aria-hidden="true"></i> fa-frown-o') ),
('fa-futbol-o', mark_safe('<i class="fa fa-futbol-o" aria-hidden="true"></i> fa-futbol-o') ),
('fa-gamepad', mark_safe('<i class="fa fa-gamepad" aria-hidden="true"></i> fa-gamepad') ),
('fa-gavel', mark_safe('<i class="fa fa-gavel" aria-hidden="true"></i> fa-gavel') ),
('fa-gbp', mark_safe('<i class="fa fa-gbp" aria-hidden="true"></i> fa-gbp') ),
('fa-ge (alias)', mark_safe('<i class="fa fa-ge (alias)" aria-hidden="true"></i> fa-ge (alias)') ),
('fa-gear (alias)', mark_safe('<i class="fa fa-gear (alias)" aria-hidden="true"></i> fa-gear (alias)') ),
('fa-gears (alias)', mark_safe('<i class="fa fa-gears (alias)" aria-hidden="true"></i> fa-gears (alias)') ),
('fa-genderless', mark_safe('<i class="fa fa-genderless" aria-hidden="true"></i> fa-genderless') ),
('fa-get-pocket', mark_safe('<i class="fa fa-get-pocket" aria-hidden="true"></i> fa-get-pocket') ),
('fa-gg', mark_safe('<i class="fa fa-gg" aria-hidden="true"></i> fa-gg') ),
('fa-gg-circle', mark_safe('<i class="fa fa-gg-circle" aria-hidden="true"></i> fa-gg-circle') ),
('fa-gift', mark_safe('<i class="fa fa-gift" aria-hidden="true"></i> fa-gift') ),
('fa-git', mark_safe('<i class="fa fa-git" aria-hidden="true"></i> fa-git') ),
('fa-git-square', mark_safe('<i class="fa fa-git-square" aria-hidden="true"></i> fa-git-square') ),
('fa-github', mark_safe('<i class="fa fa-github" aria-hidden="true"></i> fa-github') ),
('fa-github-alt', mark_safe('<i class="fa fa-github-alt" aria-hidden="true"></i> fa-github-alt') ),
('fa-github-square', mark_safe('<i class="fa fa-github-square" aria-hidden="true"></i> fa-github-square') ),
('fa-gitlab', mark_safe('<i class="fa fa-gitlab" aria-hidden="true"></i> fa-gitlab') ),
('fa-gittip (alias)', mark_safe('<i class="fa fa-gittip (alias)" aria-hidden="true"></i> fa-gittip (alias)') ),
('fa-glass', mark_safe('<i class="fa fa-glass" aria-hidden="true"></i> fa-glass') ),
('fa-glide', mark_safe('<i class="fa fa-glide" aria-hidden="true"></i> fa-glide') ),
('fa-glide-g', mark_safe('<i class="fa fa-glide-g" aria-hidden="true"></i> fa-glide-g') ),
('fa-globe', mark_safe('<i class="fa fa-globe" aria-hidden="true"></i> fa-globe') ),
('fa-google', mark_safe('<i class="fa fa-google" aria-hidden="true"></i> fa-google') ),
('fa-google-plus', mark_safe('<i class="fa fa-google-plus" aria-hidden="true"></i> fa-google-plus') ),
('fa-google-plus-circle (alias)', mark_safe('<i class="fa fa-google-plus-circle (alias)" aria-hidden="true"></i> fa-google-plus-circle (alias)') ),
('fa-google-plus-official', mark_safe('<i class="fa fa-google-plus-official" aria-hidden="true"></i> fa-google-plus-official') ),
('fa-google-plus-square', mark_safe('<i class="fa fa-google-plus-square" aria-hidden="true"></i> fa-google-plus-square') ),
('fa-google-wallet', mark_safe('<i class="fa fa-google-wallet" aria-hidden="true"></i> fa-google-wallet') ),
('fa-graduation-cap', mark_safe('<i class="fa fa-graduation-cap" aria-hidden="true"></i> fa-graduation-cap') ),
('fa-gratipay', mark_safe('<i class="fa fa-gratipay" aria-hidden="true"></i> fa-gratipay') ),
('fa-grav', mark_safe('<i class="fa fa-grav" aria-hidden="true"></i> fa-grav') ),
('fa-group (alias)', mark_safe('<i class="fa fa-group (alias)" aria-hidden="true"></i> fa-group (alias)') ),
('fa-h-square', mark_safe('<i class="fa fa-h-square" aria-hidden="true"></i> fa-h-square') ),
('fa-hacker-news', mark_safe('<i class="fa fa-hacker-news" aria-hidden="true"></i> fa-hacker-news') ),
('fa-hand-grab-o (alias)', mark_safe('<i class="fa fa-hand-grab-o (alias)" aria-hidden="true"></i> fa-hand-grab-o (alias)') ),
('fa-hand-lizard-o', mark_safe('<i class="fa fa-hand-lizard-o" aria-hidden="true"></i> fa-hand-lizard-o') ),
('fa-hand-o-down', mark_safe('<i class="fa fa-hand-o-down" aria-hidden="true"></i> fa-hand-o-down') ),
('fa-hand-o-left', mark_safe('<i class="fa fa-hand-o-left" aria-hidden="true"></i> fa-hand-o-left') ),
('fa-hand-o-right', mark_safe('<i class="fa fa-hand-o-right" aria-hidden="true"></i> fa-hand-o-right') ),
('fa-hand-o-up', mark_safe('<i class="fa fa-hand-o-up" aria-hidden="true"></i> fa-hand-o-up') ),
('fa-hand-paper-o', mark_safe('<i class="fa fa-hand-paper-o" aria-hidden="true"></i> fa-hand-paper-o') ),
('fa-hand-peace-o', mark_safe('<i class="fa fa-hand-peace-o" aria-hidden="true"></i> fa-hand-peace-o') ),
('fa-hand-pointer-o', mark_safe('<i class="fa fa-hand-pointer-o" aria-hidden="true"></i> fa-hand-pointer-o') ),
('fa-hand-rock-o', mark_safe('<i class="fa fa-hand-rock-o" aria-hidden="true"></i> fa-hand-rock-o') ),
('fa-hand-scissors-o', mark_safe('<i class="fa fa-hand-scissors-o" aria-hidden="true"></i> fa-hand-scissors-o') ),
('fa-hand-spock-o', mark_safe('<i class="fa fa-hand-spock-o" aria-hidden="true"></i> fa-hand-spock-o') ),
('fa-hand-stop-o (alias)', mark_safe('<i class="fa fa-hand-stop-o (alias)" aria-hidden="true"></i> fa-hand-stop-o (alias)') ),
('fa-handshake-o', mark_safe('<i class="fa fa-handshake-o" aria-hidden="true"></i> fa-handshake-o') ),
('fa-hard-of-hearing (alias)', mark_safe('<i class="fa fa-hard-of-hearing (alias)" aria-hidden="true"></i> fa-hard-of-hearing (alias)') ),
('fa-hashtag', mark_safe('<i class="fa fa-hashtag" aria-hidden="true"></i> fa-hashtag') ),
('fa-hdd-o', mark_safe('<i class="fa fa-hdd-o" aria-hidden="true"></i> fa-hdd-o') ),
('fa-header', mark_safe('<i class="fa fa-header" aria-hidden="true"></i> fa-header') ),
('fa-headphones', mark_safe('<i class="fa fa-headphones" aria-hidden="true"></i> fa-headphones') ),
('fa-heart', mark_safe('<i class="fa fa-heart" aria-hidden="true"></i> fa-heart') ),
('fa-heart-o', mark_safe('<i class="fa fa-heart-o" aria-hidden="true"></i> fa-heart-o') ),
('fa-heartbeat', mark_safe('<i class="fa fa-heartbeat" aria-hidden="true"></i> fa-heartbeat') ),
('fa-history', mark_safe('<i class="fa fa-history" aria-hidden="true"></i> fa-history') ),
('fa-home', mark_safe('<i class="fa fa-home" aria-hidden="true"></i> fa-home') ),
('fa-hospital-o', mark_safe('<i class="fa fa-hospital-o" aria-hidden="true"></i> fa-hospital-o') ),
('fa-hotel (alias)', mark_safe('<i class="fa fa-hotel (alias)" aria-hidden="true"></i> fa-hotel (alias)') ),
('fa-hourglass', mark_safe('<i class="fa fa-hourglass" aria-hidden="true"></i> fa-hourglass') ),
('fa-hourglass-1 (alias)', mark_safe('<i class="fa fa-hourglass-1 (alias)" aria-hidden="true"></i> fa-hourglass-1 (alias)') ),
('fa-hourglass-2 (alias)', mark_safe('<i class="fa fa-hourglass-2 (alias)" aria-hidden="true"></i> fa-hourglass-2 (alias)') ),
('fa-hourglass-3 (alias)', mark_safe('<i class="fa fa-hourglass-3 (alias)" aria-hidden="true"></i> fa-hourglass-3 (alias)') ),
('fa-hourglass-end', mark_safe('<i class="fa fa-hourglass-end" aria-hidden="true"></i> fa-hourglass-end') ),
('fa-hourglass-half', mark_safe('<i class="fa fa-hourglass-half" aria-hidden="true"></i> fa-hourglass-half') ),
('fa-hourglass-o', mark_safe('<i class="fa fa-hourglass-o" aria-hidden="true"></i> fa-hourglass-o') ),
('fa-hourglass-start', mark_safe('<i class="fa fa-hourglass-start" aria-hidden="true"></i> fa-hourglass-start') ),
('fa-houzz', mark_safe('<i class="fa fa-houzz" aria-hidden="true"></i> fa-houzz') ),
('fa-html5', mark_safe('<i class="fa fa-html5" aria-hidden="true"></i> fa-html5') ),
('fa-i-cursor', mark_safe('<i class="fa fa-i-cursor" aria-hidden="true"></i> fa-i-cursor') ),
('fa-id-badge', mark_safe('<i class="fa fa-id-badge" aria-hidden="true"></i> fa-id-badge') ),
('fa-id-card', mark_safe('<i class="fa fa-id-card" aria-hidden="true"></i> fa-id-card') ),
('fa-id-card-o', mark_safe('<i class="fa fa-id-card-o" aria-hidden="true"></i> fa-id-card-o') ),
('fa-ils', mark_safe('<i class="fa fa-ils" aria-hidden="true"></i> fa-ils') ),
('fa-image (alias)', mark_safe('<i class="fa fa-image (alias)" aria-hidden="true"></i> fa-image (alias)') ),
('fa-imdb', mark_safe('<i class="fa fa-imdb" aria-hidden="true"></i> fa-imdb') ),
('fa-inbox', mark_safe('<i class="fa fa-inbox" aria-hidden="true"></i> fa-inbox') ),
('fa-indent', mark_safe('<i class="fa fa-indent" aria-hidden="true"></i> fa-indent') ),
('fa-industry', mark_safe('<i class="fa fa-industry" aria-hidden="true"></i> fa-industry') ),
('fa-info', mark_safe('<i class="fa fa-info" aria-hidden="true"></i> fa-info') ),
('fa-info-circle', mark_safe('<i class="fa fa-info-circle" aria-hidden="true"></i> fa-info-circle') ),
('fa-inr', mark_safe('<i class="fa fa-inr" aria-hidden="true"></i> fa-inr') ),
('fa-instagram', mark_safe('<i class="fa fa-instagram" aria-hidden="true"></i> fa-instagram') ),
('fa-institution (alias)', mark_safe('<i class="fa fa-institution (alias)" aria-hidden="true"></i> fa-institution (alias)') ),
('fa-internet-explorer', mark_safe('<i class="fa fa-internet-explorer" aria-hidden="true"></i> fa-internet-explorer') ),
('fa-intersex (alias)', mark_safe('<i class="fa fa-intersex (alias)" aria-hidden="true"></i> fa-intersex (alias)') ),
('fa-ioxhost', mark_safe('<i class="fa fa-ioxhost" aria-hidden="true"></i> fa-ioxhost') ),
('fa-italic', mark_safe('<i class="fa fa-italic" aria-hidden="true"></i> fa-italic') ),
('fa-joomla', mark_safe('<i class="fa fa-joomla" aria-hidden="true"></i> fa-joomla') ),
('fa-jpy', mark_safe('<i class="fa fa-jpy" aria-hidden="true"></i> fa-jpy') ),
('fa-jsfiddle', mark_safe('<i class="fa fa-jsfiddle" aria-hidden="true"></i> fa-jsfiddle') ),
('fa-key', mark_safe('<i class="fa fa-key" aria-hidden="true"></i> fa-key') ),
('fa-keyboard-o', mark_safe('<i class="fa fa-keyboard-o" aria-hidden="true"></i> fa-keyboard-o') ),
('fa-krw', mark_safe('<i class="fa fa-krw" aria-hidden="true"></i> fa-krw') ),
('fa-language', mark_safe('<i class="fa fa-language" aria-hidden="true"></i> fa-language') ),
('fa-laptop', mark_safe('<i class="fa fa-laptop" aria-hidden="true"></i> fa-laptop') ),
('fa-lastfm', mark_safe('<i class="fa fa-lastfm" aria-hidden="true"></i> fa-lastfm') ),
('fa-lastfm-square', mark_safe('<i class="fa fa-lastfm-square" aria-hidden="true"></i> fa-lastfm-square') ),
('fa-leaf', mark_safe('<i class="fa fa-leaf" aria-hidden="true"></i> fa-leaf') ),
('fa-leanpub', mark_safe('<i class="fa fa-leanpub" aria-hidden="true"></i> fa-leanpub') ),
('fa-legal (alias)', mark_safe('<i class="fa fa-legal (alias)" aria-hidden="true"></i> fa-legal (alias)') ),
('fa-lemon-o', mark_safe('<i class="fa fa-lemon-o" aria-hidden="true"></i> fa-lemon-o') ),
('fa-level-down', mark_safe('<i class="fa fa-level-down" aria-hidden="true"></i> fa-level-down') ),
('fa-level-up', mark_safe('<i class="fa fa-level-up" aria-hidden="true"></i> fa-level-up') ),
('fa-life-bouy (alias)', mark_safe('<i class="fa fa-life-bouy (alias)" aria-hidden="true"></i> fa-life-bouy (alias)') ),
('fa-life-buoy (alias)', mark_safe('<i class="fa fa-life-buoy (alias)" aria-hidden="true"></i> fa-life-buoy (alias)') ),
('fa-life-ring', mark_safe('<i class="fa fa-life-ring" aria-hidden="true"></i> fa-life-ring') ),
('fa-life-saver (alias)', mark_safe('<i class="fa fa-life-saver (alias)" aria-hidden="true"></i> fa-life-saver (alias)') ),
('fa-lightbulb-o', mark_safe('<i class="fa fa-lightbulb-o" aria-hidden="true"></i> fa-lightbulb-o') ),
('fa-line-chart', mark_safe('<i class="fa fa-line-chart" aria-hidden="true"></i> fa-line-chart') ),
('fa-link', mark_safe('<i class="fa fa-link" aria-hidden="true"></i> fa-link') ),
('fa-linkedin', mark_safe('<i class="fa fa-linkedin" aria-hidden="true"></i> fa-linkedin') ),
('fa-linkedin-square', mark_safe('<i class="fa fa-linkedin-square" aria-hidden="true"></i> fa-linkedin-square') ),
('fa-linode', mark_safe('<i class="fa fa-linode" aria-hidden="true"></i> fa-linode') ),
('fa-linux', mark_safe('<i class="fa fa-linux" aria-hidden="true"></i> fa-linux') ),
('fa-list', mark_safe('<i class="fa fa-list" aria-hidden="true"></i> fa-list') ),
('fa-list-alt', mark_safe('<i class="fa fa-list-alt" aria-hidden="true"></i> fa-list-alt') ),
('fa-list-ol', mark_safe('<i class="fa fa-list-ol" aria-hidden="true"></i> fa-list-ol') ),
('fa-list-ul', mark_safe('<i class="fa fa-list-ul" aria-hidden="true"></i> fa-list-ul') ),
('fa-location-arrow', mark_safe('<i class="fa fa-location-arrow" aria-hidden="true"></i> fa-location-arrow') ),
('fa-lock', mark_safe('<i class="fa fa-lock" aria-hidden="true"></i> fa-lock') ),
('fa-long-arrow-down', mark_safe('<i class="fa fa-long-arrow-down" aria-hidden="true"></i> fa-long-arrow-down') ),
('fa-long-arrow-left', mark_safe('<i class="fa fa-long-arrow-left" aria-hidden="true"></i> fa-long-arrow-left') ),
('fa-long-arrow-right', mark_safe('<i class="fa fa-long-arrow-right" aria-hidden="true"></i> fa-long-arrow-right') ),
('fa-long-arrow-up', mark_safe('<i class="fa fa-long-arrow-up" aria-hidden="true"></i> fa-long-arrow-up') ),
('fa-low-vision', mark_safe('<i class="fa fa-low-vision" aria-hidden="true"></i> fa-low-vision') ),
('fa-magic', mark_safe('<i class="fa fa-magic" aria-hidden="true"></i> fa-magic') ),
('fa-magnet', mark_safe('<i class="fa fa-magnet" aria-hidden="true"></i> fa-magnet') ),
('fa-mail-forward (alias)', mark_safe('<i class="fa fa-mail-forward (alias)" aria-hidden="true"></i> fa-mail-forward (alias)') ),
('fa-mail-reply (alias)', mark_safe('<i class="fa fa-mail-reply (alias)" aria-hidden="true"></i> fa-mail-reply (alias)') ),
('fa-mail-reply-all (alias)', mark_safe('<i class="fa fa-mail-reply-all (alias)" aria-hidden="true"></i> fa-mail-reply-all (alias)') ),
('fa-male', mark_safe('<i class="fa fa-male" aria-hidden="true"></i> fa-male') ),
('fa-map', mark_safe('<i class="fa fa-map" aria-hidden="true"></i> fa-map') ),
('fa-map-marker', mark_safe('<i class="fa fa-map-marker" aria-hidden="true"></i> fa-map-marker') ),
('fa-map-o', mark_safe('<i class="fa fa-map-o" aria-hidden="true"></i> fa-map-o') ),
('fa-map-pin', mark_safe('<i class="fa fa-map-pin" aria-hidden="true"></i> fa-map-pin') ),
('fa-map-signs', mark_safe('<i class="fa fa-map-signs" aria-hidden="true"></i> fa-map-signs') ),
('fa-mars', mark_safe('<i class="fa fa-mars" aria-hidden="true"></i> fa-mars') ),
('fa-mars-double', mark_safe('<i class="fa fa-mars-double" aria-hidden="true"></i> fa-mars-double') ),
('fa-mars-stroke', mark_safe('<i class="fa fa-mars-stroke" aria-hidden="true"></i> fa-mars-stroke') ),
('fa-mars-stroke-h', mark_safe('<i class="fa fa-mars-stroke-h" aria-hidden="true"></i> fa-mars-stroke-h') ),
('fa-mars-stroke-v', mark_safe('<i class="fa fa-mars-stroke-v" aria-hidden="true"></i> fa-mars-stroke-v') ),
('fa-maxcdn', mark_safe('<i class="fa fa-maxcdn" aria-hidden="true"></i> fa-maxcdn') ),
('fa-meanpath', mark_safe('<i class="fa fa-meanpath" aria-hidden="true"></i> fa-meanpath') ),
('fa-medium', mark_safe('<i class="fa fa-medium" aria-hidden="true"></i> fa-medium') ),
('fa-medkit', mark_safe('<i class="fa fa-medkit" aria-hidden="true"></i> fa-medkit') ),
('fa-meetup', mark_safe('<i class="fa fa-meetup" aria-hidden="true"></i> fa-meetup') ),
('fa-meh-o', mark_safe('<i class="fa fa-meh-o" aria-hidden="true"></i> fa-meh-o') ),
('fa-mercury', mark_safe('<i class="fa fa-mercury" aria-hidden="true"></i> fa-mercury') ),
('fa-microchip', mark_safe('<i class="fa fa-microchip" aria-hidden="true"></i> fa-microchip') ),
('fa-microphone', mark_safe('<i class="fa fa-microphone" aria-hidden="true"></i> fa-microphone') ),
('fa-microphone-slash', mark_safe('<i class="fa fa-microphone-slash" aria-hidden="true"></i> fa-microphone-slash') ),
('fa-minus', mark_safe('<i class="fa fa-minus" aria-hidden="true"></i> fa-minus') ),
('fa-minus-circle', mark_safe('<i class="fa fa-minus-circle" aria-hidden="true"></i> fa-minus-circle') ),
('fa-minus-square', mark_safe('<i class="fa fa-minus-square" aria-hidden="true"></i> fa-minus-square') ),
('fa-minus-square-o', mark_safe('<i class="fa fa-minus-square-o" aria-hidden="true"></i> fa-minus-square-o') ),
('fa-mixcloud', mark_safe('<i class="fa fa-mixcloud" aria-hidden="true"></i> fa-mixcloud') ),
('fa-mobile', mark_safe('<i class="fa fa-mobile" aria-hidden="true"></i> fa-mobile') ),
('fa-mobile-phone (alias)', mark_safe('<i class="fa fa-mobile-phone (alias)" aria-hidden="true"></i> fa-mobile-phone (alias)') ),
('fa-modx', mark_safe('<i class="fa fa-modx" aria-hidden="true"></i> fa-modx') ),
('fa-money', mark_safe('<i class="fa fa-money" aria-hidden="true"></i> fa-money') ),
('fa-moon-o', mark_safe('<i class="fa fa-moon-o" aria-hidden="true"></i> fa-moon-o') ),
('fa-mortar-board (alias)', mark_safe('<i class="fa fa-mortar-board (alias)" aria-hidden="true"></i> fa-mortar-board (alias)') ),
('fa-motorcycle', mark_safe('<i class="fa fa-motorcycle" aria-hidden="true"></i> fa-motorcycle') ),
('fa-mouse-pointer', mark_safe('<i class="fa fa-mouse-pointer" aria-hidden="true"></i> fa-mouse-pointer') ),
('fa-music', mark_safe('<i class="fa fa-music" aria-hidden="true"></i> fa-music') ),
('fa-navicon (alias)', mark_safe('<i class="fa fa-navicon (alias)" aria-hidden="true"></i> fa-navicon (alias)') ),
('fa-neuter', mark_safe('<i class="fa fa-neuter" aria-hidden="true"></i> fa-neuter') ),
('fa-newspaper-o', mark_safe('<i class="fa fa-newspaper-o" aria-hidden="true"></i> fa-newspaper-o') ),
('fa-object-group', mark_safe('<i class="fa fa-object-group" aria-hidden="true"></i> fa-object-group') ),
('fa-object-ungroup', mark_safe('<i class="fa fa-object-ungroup" aria-hidden="true"></i> fa-object-ungroup') ),
('fa-odnoklassniki', mark_safe('<i class="fa fa-odnoklassniki" aria-hidden="true"></i> fa-odnoklassniki') ),
('fa-odnoklassniki-square', mark_safe('<i class="fa fa-odnoklassniki-square" aria-hidden="true"></i> fa-odnoklassniki-square') ),
('fa-opencart', mark_safe('<i class="fa fa-opencart" aria-hidden="true"></i> fa-opencart') ),
('fa-openid', mark_safe('<i class="fa fa-openid" aria-hidden="true"></i> fa-openid') ),
('fa-opera', mark_safe('<i class="fa fa-opera" aria-hidden="true"></i> fa-opera') ),
('fa-optin-monster', mark_safe('<i class="fa fa-optin-monster" aria-hidden="true"></i> fa-optin-monster') ),
('fa-outdent', mark_safe('<i class="fa fa-outdent" aria-hidden="true"></i> fa-outdent') ),
('fa-pagelines', mark_safe('<i class="fa fa-pagelines" aria-hidden="true"></i> fa-pagelines') ),
('fa-paint-brush', mark_safe('<i class="fa fa-paint-brush" aria-hidden="true"></i> fa-paint-brush') ),
('fa-paper-plane', mark_safe('<i class="fa fa-paper-plane" aria-hidden="true"></i> fa-paper-plane') ),
('fa-paper-plane-o', mark_safe('<i class="fa fa-paper-plane-o" aria-hidden="true"></i> fa-paper-plane-o') ),
('fa-paperclip', mark_safe('<i class="fa fa-paperclip" aria-hidden="true"></i> fa-paperclip') ),
('fa-paragraph', mark_safe('<i class="fa fa-paragraph" aria-hidden="true"></i> fa-paragraph') ),
('fa-paste (alias)', mark_safe('<i class="fa fa-paste (alias)" aria-hidden="true"></i> fa-paste (alias)') ),
('fa-pause', mark_safe('<i class="fa fa-pause" aria-hidden="true"></i> fa-pause') ),
('fa-pause-circle', mark_safe('<i class="fa fa-pause-circle" aria-hidden="true"></i> fa-pause-circle') ),
('fa-pause-circle-o', mark_safe('<i class="fa fa-pause-circle-o" aria-hidden="true"></i> fa-pause-circle-o') ),
('fa-paw', mark_safe('<i class="fa fa-paw" aria-hidden="true"></i> fa-paw') ),
('fa-paypal', mark_safe('<i class="fa fa-paypal" aria-hidden="true"></i> fa-paypal') ),
('fa-pencil', mark_safe('<i class="fa fa-pencil" aria-hidden="true"></i> fa-pencil') ),
('fa-pencil-square', mark_safe('<i class="fa fa-pencil-square" aria-hidden="true"></i> fa-pencil-square') ),
('fa-pencil-square-o', mark_safe('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> fa-pencil-square-o') ),
('fa-percent', mark_safe('<i class="fa fa-percent" aria-hidden="true"></i> fa-percent') ),
('fa-phone', mark_safe('<i class="fa fa-phone" aria-hidden="true"></i> fa-phone') ),
('fa-phone-square', mark_safe('<i class="fa fa-phone-square" aria-hidden="true"></i> fa-phone-square') ),
('fa-photo (alias)', mark_safe('<i class="fa fa-photo (alias)" aria-hidden="true"></i> fa-photo (alias)') ),
('fa-picture-o', mark_safe('<i class="fa fa-picture-o" aria-hidden="true"></i> fa-picture-o') ),
('fa-pie-chart', mark_safe('<i class="fa fa-pie-chart" aria-hidden="true"></i> fa-pie-chart') ),
('fa-pied-piper', mark_safe('<i class="fa fa-pied-piper" aria-hidden="true"></i> fa-pied-piper') ),
('fa-pied-piper-alt', mark_safe('<i class="fa fa-pied-piper-alt" aria-hidden="true"></i> fa-pied-piper-alt') ),
('fa-pied-piper-pp', mark_safe('<i class="fa fa-pied-piper-pp" aria-hidden="true"></i> fa-pied-piper-pp') ),
('fa-pinterest', mark_safe('<i class="fa fa-pinterest" aria-hidden="true"></i> fa-pinterest') ),
('fa-pinterest-p', mark_safe('<i class="fa fa-pinterest-p" aria-hidden="true"></i> fa-pinterest-p') ),
('fa-pinterest-square', mark_safe('<i class="fa fa-pinterest-square" aria-hidden="true"></i> fa-pinterest-square') ),
('fa-plane', mark_safe('<i class="fa fa-plane" aria-hidden="true"></i> fa-plane') ),
('fa-play', mark_safe('<i class="fa fa-play" aria-hidden="true"></i> fa-play') ),
('fa-play-circle', mark_safe('<i class="fa fa-play-circle" aria-hidden="true"></i> fa-play-circle') ),
('fa-play-circle-o', mark_safe('<i class="fa fa-play-circle-o" aria-hidden="true"></i> fa-play-circle-o') ),
('fa-plug', mark_safe('<i class="fa fa-plug" aria-hidden="true"></i> fa-plug') ),
('fa-plus', mark_safe('<i class="fa fa-plus" aria-hidden="true"></i> fa-plus') ),
('fa-plus-circle', mark_safe('<i class="fa fa-plus-circle" aria-hidden="true"></i> fa-plus-circle') ),
('fa-plus-square', mark_safe('<i class="fa fa-plus-square" aria-hidden="true"></i> fa-plus-square') ),
('fa-plus-square-o', mark_safe('<i class="fa fa-plus-square-o" aria-hidden="true"></i> fa-plus-square-o') ),
('fa-podcast', mark_safe('<i class="fa fa-podcast" aria-hidden="true"></i> fa-podcast') ),
('fa-power-off', mark_safe('<i class="fa fa-power-off" aria-hidden="true"></i> fa-power-off') ),
('fa-print', mark_safe('<i class="fa fa-print" aria-hidden="true"></i> fa-print') ),
('fa-product-hunt', mark_safe('<i class="fa fa-product-hunt" aria-hidden="true"></i> fa-product-hunt') ),
('fa-puzzle-piece', mark_safe('<i class="fa fa-puzzle-piece" aria-hidden="true"></i> fa-puzzle-piece') ),
('fa-qq', mark_safe('<i class="fa fa-qq" aria-hidden="true"></i> fa-qq') ),
('fa-qrcode', mark_safe('<i class="fa fa-qrcode" aria-hidden="true"></i> fa-qrcode') ),
('fa-question', mark_safe('<i class="fa fa-question" aria-hidden="true"></i> fa-question') ),
('fa-question-circle', mark_safe('<i class="fa fa-question-circle" aria-hidden="true"></i> fa-question-circle') ),
('fa-question-circle-o', mark_safe('<i class="fa fa-question-circle-o" aria-hidden="true"></i> fa-question-circle-o') ),
('fa-quora', mark_safe('<i class="fa fa-quora" aria-hidden="true"></i> fa-quora') ),
('fa-quote-left', mark_safe('<i class="fa fa-quote-left" aria-hidden="true"></i> fa-quote-left') ),
('fa-quote-right', mark_safe('<i class="fa fa-quote-right" aria-hidden="true"></i> fa-quote-right') ),
('fa-ra (alias)', mark_safe('<i class="fa fa-ra (alias)" aria-hidden="true"></i> fa-ra (alias)') ),
('fa-random', mark_safe('<i class="fa fa-random" aria-hidden="true"></i> fa-random') ),
('fa-ravelry', mark_safe('<i class="fa fa-ravelry" aria-hidden="true"></i> fa-ravelry') ),
('fa-rebel', mark_safe('<i class="fa fa-rebel" aria-hidden="true"></i> fa-rebel') ),
('fa-recycle', mark_safe('<i class="fa fa-recycle" aria-hidden="true"></i> fa-recycle') ),
('fa-reddit', mark_safe('<i class="fa fa-reddit" aria-hidden="true"></i> fa-reddit') ),
('fa-reddit-alien', mark_safe('<i class="fa fa-reddit-alien" aria-hidden="true"></i> fa-reddit-alien') ),
('fa-reddit-square', mark_safe('<i class="fa fa-reddit-square" aria-hidden="true"></i> fa-reddit-square') ),
('fa-refresh', mark_safe('<i class="fa fa-refresh" aria-hidden="true"></i> fa-refresh') ),
('fa-registered', mark_safe('<i class="fa fa-registered" aria-hidden="true"></i> fa-registered') ),
('fa-remove (alias)', mark_safe('<i class="fa fa-remove (alias)" aria-hidden="true"></i> fa-remove (alias)') ),
('fa-renren', mark_safe('<i class="fa fa-renren" aria-hidden="true"></i> fa-renren') ),
('fa-reorder (alias)', mark_safe('<i class="fa fa-reorder (alias)" aria-hidden="true"></i> fa-reorder (alias)') ),
('fa-repeat', mark_safe('<i class="fa fa-repeat" aria-hidden="true"></i> fa-repeat') ),
('fa-reply', mark_safe('<i class="fa fa-reply" aria-hidden="true"></i> fa-reply') ),
('fa-reply-all', mark_safe('<i class="fa fa-reply-all" aria-hidden="true"></i> fa-reply-all') ),
('fa-resistance (alias)', mark_safe('<i class="fa fa-resistance (alias)" aria-hidden="true"></i> fa-resistance (alias)') ),
('fa-retweet', mark_safe('<i class="fa fa-retweet" aria-hidden="true"></i> fa-retweet') ),
('fa-rmb (alias)', mark_safe('<i class="fa fa-rmb (alias)" aria-hidden="true"></i> fa-rmb (alias)') ),
('fa-road', mark_safe('<i class="fa fa-road" aria-hidden="true"></i> fa-road') ),
('fa-rocket', mark_safe('<i class="fa fa-rocket" aria-hidden="true"></i> fa-rocket') ),
('fa-rotate-left (alias)', mark_safe('<i class="fa fa-rotate-left (alias)" aria-hidden="true"></i> fa-rotate-left (alias)') ),
('fa-rotate-right (alias)', mark_safe('<i class="fa fa-rotate-right (alias)" aria-hidden="true"></i> fa-rotate-right (alias)') ),
('fa-rouble (alias)', mark_safe('<i class="fa fa-rouble (alias)" aria-hidden="true"></i> fa-rouble (alias)') ),
('fa-rss', mark_safe('<i class="fa fa-rss" aria-hidden="true"></i> fa-rss') ),
('fa-rss-square', mark_safe('<i class="fa fa-rss-square" aria-hidden="true"></i> fa-rss-square') ),
('fa-rub', mark_safe('<i class="fa fa-rub" aria-hidden="true"></i> fa-rub') ),
('fa-ruble (alias)', mark_safe('<i class="fa fa-ruble (alias)" aria-hidden="true"></i> fa-ruble (alias)') ),
('fa-rupee (alias)', mark_safe('<i class="fa fa-rupee (alias)" aria-hidden="true"></i> fa-rupee (alias)') ),
('fa-s15 (alias)', mark_safe('<i class="fa fa-s15 (alias)" aria-hidden="true"></i> fa-s15 (alias)') ),
('fa-safari', mark_safe('<i class="fa fa-safari" aria-hidden="true"></i> fa-safari') ),
('fa-save (alias)', mark_safe('<i class="fa fa-save (alias)" aria-hidden="true"></i> fa-save (alias)') ),
('fa-scissors', mark_safe('<i class="fa fa-scissors" aria-hidden="true"></i> fa-scissors') ),
('fa-scribd', mark_safe('<i class="fa fa-scribd" aria-hidden="true"></i> fa-scribd') ),
('fa-search', mark_safe('<i class="fa fa-search" aria-hidden="true"></i> fa-search') ),
('fa-search-minus', mark_safe('<i class="fa fa-search-minus" aria-hidden="true"></i> fa-search-minus') ),
('fa-search-plus', mark_safe('<i class="fa fa-search-plus" aria-hidden="true"></i> fa-search-plus') ),
('fa-sellsy', mark_safe('<i class="fa fa-sellsy" aria-hidden="true"></i> fa-sellsy') ),
('fa-send (alias)', mark_safe('<i class="fa fa-send (alias)" aria-hidden="true"></i> fa-send (alias)') ),
('fa-send-o (alias)', mark_safe('<i class="fa fa-send-o (alias)" aria-hidden="true"></i> fa-send-o (alias)') ),
('fa-server', mark_safe('<i class="fa fa-server" aria-hidden="true"></i> fa-server') ),
('fa-share', mark_safe('<i class="fa fa-share" aria-hidden="true"></i> fa-share') ),
('fa-share-alt', mark_safe('<i class="fa fa-share-alt" aria-hidden="true"></i> fa-share-alt') ),
('fa-share-alt-square', mark_safe('<i class="fa fa-share-alt-square" aria-hidden="true"></i> fa-share-alt-square') ),
('fa-share-square', mark_safe('<i class="fa fa-share-square" aria-hidden="true"></i> fa-share-square') ),
('fa-share-square-o', mark_safe('<i class="fa fa-share-square-o" aria-hidden="true"></i> fa-share-square-o') ),
('fa-shekel (alias)', mark_safe('<i class="fa fa-shekel (alias)" aria-hidden="true"></i> fa-shekel (alias)') ),
('fa-sheqel (alias)', mark_safe('<i class="fa fa-sheqel (alias)" aria-hidden="true"></i> fa-sheqel (alias)') ),
('fa-shield', mark_safe('<i class="fa fa-shield" aria-hidden="true"></i> fa-shield') ),
('fa-ship', mark_safe('<i class="fa fa-ship" aria-hidden="true"></i> fa-ship') ),
('fa-shirtsinbulk', mark_safe('<i class="fa fa-shirtsinbulk" aria-hidden="true"></i> fa-shirtsinbulk') ),
('fa-shopping-bag', mark_safe('<i class="fa fa-shopping-bag" aria-hidden="true"></i> fa-shopping-bag') ),
('fa-shopping-basket', mark_safe('<i class="fa fa-shopping-basket" aria-hidden="true"></i> fa-shopping-basket') ),
('fa-shopping-cart', mark_safe('<i class="fa fa-shopping-cart" aria-hidden="true"></i> fa-shopping-cart') ),
('fa-shower', mark_safe('<i class="fa fa-shower" aria-hidden="true"></i> fa-shower') ),
('fa-sign-in', mark_safe('<i class="fa fa-sign-in" aria-hidden="true"></i> fa-sign-in') ),
('fa-sign-language', mark_safe('<i class="fa fa-sign-language" aria-hidden="true"></i> fa-sign-language') ),
('fa-sign-out', mark_safe('<i class="fa fa-sign-out" aria-hidden="true"></i> fa-sign-out') ),
('fa-signal', mark_safe('<i class="fa fa-signal" aria-hidden="true"></i> fa-signal') ),
('fa-signing (alias)', mark_safe('<i class="fa fa-signing (alias)" aria-hidden="true"></i> fa-signing (alias)') ),
('fa-simplybuilt', mark_safe('<i class="fa fa-simplybuilt" aria-hidden="true"></i> fa-simplybuilt') ),
('fa-sitemap', mark_safe('<i class="fa fa-sitemap" aria-hidden="true"></i> fa-sitemap') ),
('fa-skyatlas', mark_safe('<i class="fa fa-skyatlas" aria-hidden="true"></i> fa-skyatlas') ),
('fa-skype', mark_safe('<i class="fa fa-skype" aria-hidden="true"></i> fa-skype') ),
('fa-slack', mark_safe('<i class="fa fa-slack" aria-hidden="true"></i> fa-slack') ),
('fa-sliders', mark_safe('<i class="fa fa-sliders" aria-hidden="true"></i> fa-sliders') ),
('fa-slideshare', mark_safe('<i class="fa fa-slideshare" aria-hidden="true"></i> fa-slideshare') ),
('fa-smile-o', mark_safe('<i class="fa fa-smile-o" aria-hidden="true"></i> fa-smile-o') ),
('fa-snapchat', mark_safe('<i class="fa fa-snapchat" aria-hidden="true"></i> fa-snapchat') ),
('fa-snapchat-ghost', mark_safe('<i class="fa fa-snapchat-ghost" aria-hidden="true"></i> fa-snapchat-ghost') ),
('fa-snapchat-square', mark_safe('<i class="fa fa-snapchat-square" aria-hidden="true"></i> fa-snapchat-square') ),
('fa-snowflake-o', mark_safe('<i class="fa fa-snowflake-o" aria-hidden="true"></i> fa-snowflake-o') ),
('fa-soccer-ball-o (alias)', mark_safe('<i class="fa fa-soccer-ball-o (alias)" aria-hidden="true"></i> fa-soccer-ball-o (alias)') ),
('fa-sort', mark_safe('<i class="fa fa-sort" aria-hidden="true"></i> fa-sort') ),
('fa-sort-alpha-asc', mark_safe('<i class="fa fa-sort-alpha-asc" aria-hidden="true"></i> fa-sort-alpha-asc') ),
('fa-sort-alpha-desc', mark_safe('<i class="fa fa-sort-alpha-desc" aria-hidden="true"></i> fa-sort-alpha-desc') ),
('fa-sort-amount-asc', mark_safe('<i class="fa fa-sort-amount-asc" aria-hidden="true"></i> fa-sort-amount-asc') ),
('fa-sort-amount-desc', mark_safe('<i class="fa fa-sort-amount-desc" aria-hidden="true"></i> fa-sort-amount-desc') ),
('fa-sort-asc', mark_safe('<i class="fa fa-sort-asc" aria-hidden="true"></i> fa-sort-asc') ),
('fa-sort-desc', mark_safe('<i class="fa fa-sort-desc" aria-hidden="true"></i> fa-sort-desc') ),
('fa-sort-down (alias)', mark_safe('<i class="fa fa-sort-down (alias)" aria-hidden="true"></i> fa-sort-down (alias)') ),
('fa-sort-numeric-asc', mark_safe('<i class="fa fa-sort-numeric-asc" aria-hidden="true"></i> fa-sort-numeric-asc') ),
('fa-sort-numeric-desc', mark_safe('<i class="fa fa-sort-numeric-desc" aria-hidden="true"></i> fa-sort-numeric-desc') ),
('fa-sort-up (alias)', mark_safe('<i class="fa fa-sort-up (alias)" aria-hidden="true"></i> fa-sort-up (alias)') ),
('fa-soundcloud', mark_safe('<i class="fa fa-soundcloud" aria-hidden="true"></i> fa-soundcloud') ),
('fa-space-shuttle', mark_safe('<i class="fa fa-space-shuttle" aria-hidden="true"></i> fa-space-shuttle') ),
('fa-spinner', mark_safe('<i class="fa fa-spinner" aria-hidden="true"></i> fa-spinner') ),
('fa-spoon', mark_safe('<i class="fa fa-spoon" aria-hidden="true"></i> fa-spoon') ),
('fa-spotify', mark_safe('<i class="fa fa-spotify" aria-hidden="true"></i> fa-spotify') ),
('fa-square', mark_safe('<i class="fa fa-square" aria-hidden="true"></i> fa-square') ),
('fa-square-o', mark_safe('<i class="fa fa-square-o" aria-hidden="true"></i> fa-square-o') ),
('fa-stack-exchange', mark_safe('<i class="fa fa-stack-exchange" aria-hidden="true"></i> fa-stack-exchange') ),
('fa-stack-overflow', mark_safe('<i class="fa fa-stack-overflow" aria-hidden="true"></i> fa-stack-overflow') ),
('fa-star', mark_safe('<i class="fa fa-star" aria-hidden="true"></i> fa-star') ),
('fa-star-half', mark_safe('<i class="fa fa-star-half" aria-hidden="true"></i> fa-star-half') ),
('fa-star-half-empty (alias)', mark_safe('<i class="fa fa-star-half-empty (alias)" aria-hidden="true"></i> fa-star-half-empty (alias)') ),
('fa-star-half-full (alias)', mark_safe('<i class="fa fa-star-half-full (alias)" aria-hidden="true"></i> fa-star-half-full (alias)') ),
('fa-star-half-o', mark_safe('<i class="fa fa-star-half-o" aria-hidden="true"></i> fa-star-half-o') ),
('fa-star-o', mark_safe('<i class="fa fa-star-o" aria-hidden="true"></i> fa-star-o') ),
('fa-steam', mark_safe('<i class="fa fa-steam" aria-hidden="true"></i> fa-steam') ),
('fa-steam-square', mark_safe('<i class="fa fa-steam-square" aria-hidden="true"></i> fa-steam-square') ),
('fa-step-backward', mark_safe('<i class="fa fa-step-backward" aria-hidden="true"></i> fa-step-backward') ),
('fa-step-forward', mark_safe('<i class="fa fa-step-forward" aria-hidden="true"></i> fa-step-forward') ),
('fa-stethoscope', mark_safe('<i class="fa fa-stethoscope" aria-hidden="true"></i> fa-stethoscope') ),
('fa-sticky-note', mark_safe('<i class="fa fa-sticky-note" aria-hidden="true"></i> fa-sticky-note') ),
('fa-sticky-note-o', mark_safe('<i class="fa fa-sticky-note-o" aria-hidden="true"></i> fa-sticky-note-o') ),
('fa-stop', mark_safe('<i class="fa fa-stop" aria-hidden="true"></i> fa-stop') ),
('fa-stop-circle', mark_safe('<i class="fa fa-stop-circle" aria-hidden="true"></i> fa-stop-circle') ),
('fa-stop-circle-o', mark_safe('<i class="fa fa-stop-circle-o" aria-hidden="true"></i> fa-stop-circle-o') ),
('fa-street-view', mark_safe('<i class="fa fa-street-view" aria-hidden="true"></i> fa-street-view') ),
('fa-strikethrough', mark_safe('<i class="fa fa-strikethrough" aria-hidden="true"></i> fa-strikethrough') ),
('fa-stumbleupon', mark_safe('<i class="fa fa-stumbleupon" aria-hidden="true"></i> fa-stumbleupon') ),
('fa-stumbleupon-circle', mark_safe('<i class="fa fa-stumbleupon-circle" aria-hidden="true"></i> fa-stumbleupon-circle') ),
('fa-subscript', mark_safe('<i class="fa fa-subscript" aria-hidden="true"></i> fa-subscript') ),
('fa-subway', mark_safe('<i class="fa fa-subway" aria-hidden="true"></i> fa-subway') ),
('fa-suitcase', mark_safe('<i class="fa fa-suitcase" aria-hidden="true"></i> fa-suitcase') ),
('fa-sun-o', mark_safe('<i class="fa fa-sun-o" aria-hidden="true"></i> fa-sun-o') ),
('fa-superpowers', mark_safe('<i class="fa fa-superpowers" aria-hidden="true"></i> fa-superpowers') ),
('fa-superscript', mark_safe('<i class="fa fa-superscript" aria-hidden="true"></i> fa-superscript') ),
('fa-support (alias)', mark_safe('<i class="fa fa-support (alias)" aria-hidden="true"></i> fa-support (alias)') ),
('fa-table', mark_safe('<i class="fa fa-table" aria-hidden="true"></i> fa-table') ),
('fa-tablet', mark_safe('<i class="fa fa-tablet" aria-hidden="true"></i> fa-tablet') ),
('fa-tachometer', mark_safe('<i class="fa fa-tachometer" aria-hidden="true"></i> fa-tachometer') ),
('fa-tag', mark_safe('<i class="fa fa-tag" aria-hidden="true"></i> fa-tag') ),
('fa-tags', mark_safe('<i class="fa fa-tags" aria-hidden="true"></i> fa-tags') ),
('fa-tasks', mark_safe('<i class="fa fa-tasks" aria-hidden="true"></i> fa-tasks') ),
('fa-taxi', mark_safe('<i class="fa fa-taxi" aria-hidden="true"></i> fa-taxi') ),
('fa-telegram', mark_safe('<i class="fa fa-telegram" aria-hidden="true"></i> fa-telegram') ),
('fa-television', mark_safe('<i class="fa fa-television" aria-hidden="true"></i> fa-television') ),
('fa-tencent-weibo', mark_safe('<i class="fa fa-tencent-weibo" aria-hidden="true"></i> fa-tencent-weibo') ),
('fa-terminal', mark_safe('<i class="fa fa-terminal" aria-hidden="true"></i> fa-terminal') ),
('fa-text-height', mark_safe('<i class="fa fa-text-height" aria-hidden="true"></i> fa-text-height') ),
('fa-text-width', mark_safe('<i class="fa fa-text-width" aria-hidden="true"></i> fa-text-width') ),
('fa-th', mark_safe('<i class="fa fa-th" aria-hidden="true"></i> fa-th') ),
('fa-th-large', mark_safe('<i class="fa fa-th-large" aria-hidden="true"></i> fa-th-large') ),
('fa-th-list', mark_safe('<i class="fa fa-th-list" aria-hidden="true"></i> fa-th-list') ),
('fa-themeisle', mark_safe('<i class="fa fa-themeisle" aria-hidden="true"></i> fa-themeisle') ),
('fa-thermometer (alias)', mark_safe('<i class="fa fa-thermometer (alias)" aria-hidden="true"></i> fa-thermometer (alias)') ),
('fa-thermometer-0 (alias)', mark_safe('<i class="fa fa-thermometer-0 (alias)" aria-hidden="true"></i> fa-thermometer-0 (alias)') ),
('fa-thermometer-1 (alias)', mark_safe('<i class="fa fa-thermometer-1 (alias)" aria-hidden="true"></i> fa-thermometer-1 (alias)') ),
('fa-thermometer-2 (alias)', mark_safe('<i class="fa fa-thermometer-2 (alias)" aria-hidden="true"></i> fa-thermometer-2 (alias)') ),
('fa-thermometer-3 (alias)', mark_safe('<i class="fa fa-thermometer-3 (alias)" aria-hidden="true"></i> fa-thermometer-3 (alias)') ),
('fa-thermometer-4 (alias)', mark_safe('<i class="fa fa-thermometer-4 (alias)" aria-hidden="true"></i> fa-thermometer-4 (alias)') ),
('fa-thermometer-empty', mark_safe('<i class="fa fa-thermometer-empty" aria-hidden="true"></i> fa-thermometer-empty') ),
('fa-thermometer-full', mark_safe('<i class="fa fa-thermometer-full" aria-hidden="true"></i> fa-thermometer-full') ),
('fa-thermometer-half', mark_safe('<i class="fa fa-thermometer-half" aria-hidden="true"></i> fa-thermometer-half') ),
('fa-thermometer-quarter', mark_safe('<i class="fa fa-thermometer-quarter" aria-hidden="true"></i> fa-thermometer-quarter') ),
('fa-thermometer-three-quarters', mark_safe('<i class="fa fa-thermometer-three-quarters" aria-hidden="true"></i> fa-thermometer-three-quarters') ),
('fa-thumb-tack', mark_safe('<i class="fa fa-thumb-tack" aria-hidden="true"></i> fa-thumb-tack') ),
('fa-thumbs-down', mark_safe('<i class="fa fa-thumbs-down" aria-hidden="true"></i> fa-thumbs-down') ),
('fa-thumbs-o-down', mark_safe('<i class="fa fa-thumbs-o-down" aria-hidden="true"></i> fa-thumbs-o-down') ),
('fa-thumbs-o-up', mark_safe('<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> fa-thumbs-o-up') ),
('fa-thumbs-up', mark_safe('<i class="fa fa-thumbs-up" aria-hidden="true"></i> fa-thumbs-up') ),
('fa-ticket', mark_safe('<i class="fa fa-ticket" aria-hidden="true"></i> fa-ticket') ),
('fa-times', mark_safe('<i class="fa fa-times" aria-hidden="true"></i> fa-times') ),
('fa-times-circle', mark_safe('<i class="fa fa-times-circle" aria-hidden="true"></i> fa-times-circle') ),
('fa-times-circle-o', mark_safe('<i class="fa fa-times-circle-o" aria-hidden="true"></i> fa-times-circle-o') ),
('fa-times-rectangle (alias)', mark_safe('<i class="fa fa-times-rectangle (alias)" aria-hidden="true"></i> fa-times-rectangle (alias)') ),
('fa-times-rectangle-o (alias)', mark_safe('<i class="fa fa-times-rectangle-o (alias)" aria-hidden="true"></i> fa-times-rectangle-o (alias)') ),
('fa-tint', mark_safe('<i class="fa fa-tint" aria-hidden="true"></i> fa-tint') ),
('fa-toggle-down (alias)', mark_safe('<i class="fa fa-toggle-down (alias)" aria-hidden="true"></i> fa-toggle-down (alias)') ),
('fa-toggle-left (alias)', mark_safe('<i class="fa fa-toggle-left (alias)" aria-hidden="true"></i> fa-toggle-left (alias)') ),
('fa-toggle-off', mark_safe('<i class="fa fa-toggle-off" aria-hidden="true"></i> fa-toggle-off') ),
('fa-toggle-on', mark_safe('<i class="fa fa-toggle-on" aria-hidden="true"></i> fa-toggle-on') ),
('fa-toggle-right (alias)', mark_safe('<i class="fa fa-toggle-right (alias)" aria-hidden="true"></i> fa-toggle-right (alias)') ),
('fa-toggle-up (alias)', mark_safe('<i class="fa fa-toggle-up (alias)" aria-hidden="true"></i> fa-toggle-up (alias)') ),
('fa-trademark', mark_safe('<i class="fa fa-trademark" aria-hidden="true"></i> fa-trademark') ),
('fa-train', mark_safe('<i class="fa fa-train" aria-hidden="true"></i> fa-train') ),
('fa-transgender', mark_safe('<i class="fa fa-transgender" aria-hidden="true"></i> fa-transgender') ),
('fa-transgender-alt', mark_safe('<i class="fa fa-transgender-alt" aria-hidden="true"></i> fa-transgender-alt') ),
('fa-trash', mark_safe('<i class="fa fa-trash" aria-hidden="true"></i> fa-trash') ),
('fa-trash-o', mark_safe('<i class="fa fa-trash-o" aria-hidden="true"></i> fa-trash-o') ),
('fa-tree', mark_safe('<i class="fa fa-tree" aria-hidden="true"></i> fa-tree') ),
('fa-trello', mark_safe('<i class="fa fa-trello" aria-hidden="true"></i> fa-trello') ),
('fa-tripadvisor', mark_safe('<i class="fa fa-tripadvisor" aria-hidden="true"></i> fa-tripadvisor') ),
('fa-trophy', mark_safe('<i class="fa fa-trophy" aria-hidden="true"></i> fa-trophy') ),
('fa-truck', mark_safe('<i class="fa fa-truck" aria-hidden="true"></i> fa-truck') ),
('fa-try', mark_safe('<i class="fa fa-try" aria-hidden="true"></i> fa-try') ),
('fa-tty', mark_safe('<i class="fa fa-tty" aria-hidden="true"></i> fa-tty') ),
('fa-tumblr', mark_safe('<i class="fa fa-tumblr" aria-hidden="true"></i> fa-tumblr') ),
('fa-tumblr-square', mark_safe('<i class="fa fa-tumblr-square" aria-hidden="true"></i> fa-tumblr-square') ),
('fa-turkish-lira (alias)', mark_safe('<i class="fa fa-turkish-lira (alias)" aria-hidden="true"></i> fa-turkish-lira (alias)') ),
('fa-tv (alias)', mark_safe('<i class="fa fa-tv (alias)" aria-hidden="true"></i> fa-tv (alias)') ),
('fa-twitch', mark_safe('<i class="fa fa-twitch" aria-hidden="true"></i> fa-twitch') ),
('fa-twitter', mark_safe('<i class="fa fa-twitter" aria-hidden="true"></i> fa-twitter') ),
('fa-twitter-square', mark_safe('<i class="fa fa-twitter-square" aria-hidden="true"></i> fa-twitter-square') ),
('fa-umbrella', mark_safe('<i class="fa fa-umbrella" aria-hidden="true"></i> fa-umbrella') ),
('fa-underline', mark_safe('<i class="fa fa-underline" aria-hidden="true"></i> fa-underline') ),
('fa-undo', mark_safe('<i class="fa fa-undo" aria-hidden="true"></i> fa-undo') ),
('fa-universal-access', mark_safe('<i class="fa fa-universal-access" aria-hidden="true"></i> fa-universal-access') ),
('fa-university', mark_safe('<i class="fa fa-university" aria-hidden="true"></i> fa-university') ),
('fa-unlink (alias)', mark_safe('<i class="fa fa-unlink (alias)" aria-hidden="true"></i> fa-unlink (alias)') ),
('fa-unlock', mark_safe('<i class="fa fa-unlock" aria-hidden="true"></i> fa-unlock') ),
('fa-unlock-alt', mark_safe('<i class="fa fa-unlock-alt" aria-hidden="true"></i> fa-unlock-alt') ),
('fa-unsorted (alias)', mark_safe('<i class="fa fa-unsorted (alias)" aria-hidden="true"></i> fa-unsorted (alias)') ),
('fa-upload', mark_safe('<i class="fa fa-upload" aria-hidden="true"></i> fa-upload') ),
('fa-usb', mark_safe('<i class="fa fa-usb" aria-hidden="true"></i> fa-usb') ),
('fa-usd', mark_safe('<i class="fa fa-usd" aria-hidden="true"></i> fa-usd') ),
('fa-user', mark_safe('<i class="fa fa-user" aria-hidden="true"></i> fa-user') ),
('fa-user-circle', mark_safe('<i class="fa fa-user-circle" aria-hidden="true"></i> fa-user-circle') ),
('fa-user-circle-o', mark_safe('<i class="fa fa-user-circle-o" aria-hidden="true"></i> fa-user-circle-o') ),
('fa-user-md', mark_safe('<i class="fa fa-user-md" aria-hidden="true"></i> fa-user-md') ),
('fa-user-o', mark_safe('<i class="fa fa-user-o" aria-hidden="true"></i> fa-user-o') ),
('fa-user-plus', mark_safe('<i class="fa fa-user-plus" aria-hidden="true"></i> fa-user-plus') ),
('fa-user-secret', mark_safe('<i class="fa fa-user-secret" aria-hidden="true"></i> fa-user-secret') ),
('fa-user-times', mark_safe('<i class="fa fa-user-times" aria-hidden="true"></i> fa-user-times') ),
('fa-users', mark_safe('<i class="fa fa-users" aria-hidden="true"></i> fa-users') ),
('fa-vcard (alias)', mark_safe('<i class="fa fa-vcard (alias)" aria-hidden="true"></i> fa-vcard (alias)') ),
('fa-vcard-o (alias)', mark_safe('<i class="fa fa-vcard-o (alias)" aria-hidden="true"></i> fa-vcard-o (alias)') ),
('fa-venus', mark_safe('<i class="fa fa-venus" aria-hidden="true"></i> fa-venus') ),
('fa-venus-double', mark_safe('<i class="fa fa-venus-double" aria-hidden="true"></i> fa-venus-double') ),
('fa-venus-mars', mark_safe('<i class="fa fa-venus-mars" aria-hidden="true"></i> fa-venus-mars') ),
('fa-viacoin', mark_safe('<i class="fa fa-viacoin" aria-hidden="true"></i> fa-viacoin') ),
('fa-viadeo', mark_safe('<i class="fa fa-viadeo" aria-hidden="true"></i> fa-viadeo') ),
('fa-viadeo-square', mark_safe('<i class="fa fa-viadeo-square" aria-hidden="true"></i> fa-viadeo-square') ),
('fa-video-camera', mark_safe('<i class="fa fa-video-camera" aria-hidden="true"></i> fa-video-camera') ),
('fa-vimeo', mark_safe('<i class="fa fa-vimeo" aria-hidden="true"></i> fa-vimeo') ),
('fa-vimeo-square', mark_safe('<i class="fa fa-vimeo-square" aria-hidden="true"></i> fa-vimeo-square') ),
('fa-vine', mark_safe('<i class="fa fa-vine" aria-hidden="true"></i> fa-vine') ),
('fa-vk', mark_safe('<i class="fa fa-vk" aria-hidden="true"></i> fa-vk') ),
('fa-volume-control-phone', mark_safe('<i class="fa fa-volume-control-phone" aria-hidden="true"></i> fa-volume-control-phone') ),
('fa-volume-down', mark_safe('<i class="fa fa-volume-down" aria-hidden="true"></i> fa-volume-down') ),
('fa-volume-off', mark_safe('<i class="fa fa-volume-off" aria-hidden="true"></i> fa-volume-off') ),
('fa-volume-up', mark_safe('<i class="fa fa-volume-up" aria-hidden="true"></i> fa-volume-up') ),
('fa-warning (alias)', mark_safe('<i class="fa fa-warning (alias)" aria-hidden="true"></i> fa-warning (alias)') ),
('fa-wechat (alias)', mark_safe('<i class="fa fa-wechat (alias)" aria-hidden="true"></i> fa-wechat (alias)') ),
('fa-weibo', mark_safe('<i class="fa fa-weibo" aria-hidden="true"></i> fa-weibo') ),
('fa-weixin', mark_safe('<i class="fa fa-weixin" aria-hidden="true"></i> fa-weixin') ),
('fa-whatsapp', mark_safe('<i class="fa fa-whatsapp" aria-hidden="true"></i> fa-whatsapp') ),
('fa-wheelchair', mark_safe('<i class="fa fa-wheelchair" aria-hidden="true"></i> fa-wheelchair') ),
('fa-wheelchair-alt', mark_safe('<i class="fa fa-wheelchair-alt" aria-hidden="true"></i> fa-wheelchair-alt') ),
('fa-wifi', mark_safe('<i class="fa fa-wifi" aria-hidden="true"></i> fa-wifi') ),
('fa-wikipedia-w', mark_safe('<i class="fa fa-wikipedia-w" aria-hidden="true"></i> fa-wikipedia-w') ),
('fa-window-close', mark_safe('<i class="fa fa-window-close" aria-hidden="true"></i> fa-window-close') ),
('fa-window-close-o', mark_safe('<i class="fa fa-window-close-o" aria-hidden="true"></i> fa-window-close-o') ),
('fa-window-maximize', mark_safe('<i class="fa fa-window-maximize" aria-hidden="true"></i> fa-window-maximize') ),
('fa-window-minimize', mark_safe('<i class="fa fa-window-minimize" aria-hidden="true"></i> fa-window-minimize') ),
('fa-window-restore', mark_safe('<i class="fa fa-window-restore" aria-hidden="true"></i> fa-window-restore') ),
('fa-windows', mark_safe('<i class="fa fa-windows" aria-hidden="true"></i> fa-windows') ),
('fa-won (alias)', mark_safe('<i class="fa fa-won (alias)" aria-hidden="true"></i> fa-won (alias)') ),
('fa-wordpress', mark_safe('<i class="fa fa-wordpress" aria-hidden="true"></i> fa-wordpress') ),
('fa-wpbeginner', mark_safe('<i class="fa fa-wpbeginner" aria-hidden="true"></i> fa-wpbeginner') ),
('fa-wpexplorer', mark_safe('<i class="fa fa-wpexplorer" aria-hidden="true"></i> fa-wpexplorer') ),
('fa-wpforms', mark_safe('<i class="fa fa-wpforms" aria-hidden="true"></i> fa-wpforms') ),
('fa-wrench', mark_safe('<i class="fa fa-wrench" aria-hidden="true"></i> fa-wrench') ),
('fa-xing', mark_safe('<i class="fa fa-xing" aria-hidden="true"></i> fa-xing') ),
('fa-xing-square', mark_safe('<i class="fa fa-xing-square" aria-hidden="true"></i> fa-xing-square') ),
('fa-y-combinator', mark_safe('<i class="fa fa-y-combinator" aria-hidden="true"></i> fa-y-combinator') ),
('fa-y-combinator-square (alias)', mark_safe('<i class="fa fa-y-combinator-square (alias)" aria-hidden="true"></i> fa-y-combinator-square (alias)') ),
('fa-yahoo', mark_safe('<i class="fa fa-yahoo" aria-hidden="true"></i> fa-yahoo') ),
('fa-yc (alias)', mark_safe('<i class="fa fa-yc (alias)" aria-hidden="true"></i> fa-yc (alias)') ),
('fa-yc-square (alias)', mark_safe('<i class="fa fa-yc-square (alias)" aria-hidden="true"></i> fa-yc-square (alias)') ),
('fa-yelp', mark_safe('<i class="fa fa-yelp" aria-hidden="true"></i> fa-yelp') ),
('fa-yen (alias)', mark_safe('<i class="fa fa-yen (alias)" aria-hidden="true"></i> fa-yen (alias)') ),
('fa-yoast', mark_safe('<i class="fa fa-yoast" aria-hidden="true"></i> fa-yoast') ),
('fa-youtube', mark_safe('<i class="fa fa-youtube" aria-hidden="true"></i> fa-youtube') ),
('fa-youtube-play', mark_safe('<i class="fa fa-youtube-play" aria-hidden="true"></i> fa-youtube-play') ),
('fa-youtube-square', mark_safe('<i class="fa fa-youtube-square" aria-hidden="true"></i> fa-youtube-square') ),


)

class BaseClassManager(PolymorphicManager):
    def get_queryset(self):
        qs = super(BaseClassManager, self).get_queryset()
        return qs

class BaseClassQuerySet(PolymorphicQuerySet):
    def non_polymorphic(self):
        return self.non_polymorphic()


class BaseClass(PolymorphicModel):
    sites = models.ManyToManyField(Site)
    name_global = models.CharField(
        _('Heiti'),
        max_length=250,
        default='',
        help_text=_(
            'Heiti færslu í listum, má innihalda ritstjórnarlegar upplýsingar því heiti úr' \
            'tungumálum er notoð til birtingar á vef.')
    )
    note = models.TextField(_('Minnispunktar'), blank=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, verbose_name=_('Eigandi færslu'))
    on_site = CurrentSiteManager()
    active = models.BooleanField(default=True)

    # objects = BaseClassManager()

    def __str__(self):
        return self.name_global

    def _lang(self):
        return self.basecalltranslation_set.all()

    lang = property(_lang)

    def edit_link(self):
        return mark_safe('<a href="/admin/artworld/%s/%s/change/" target="_blank">Breyta</a>' % (self.polymorphic_ctype, self.id ) )

    # class Meta:
    #     # You must define a meta with en explicit app_label
    #     app_label = 'artworld'

    @staticmethod
    def autocomplete_search_fields():
        return  ('name_global',)

    @property
    def translations(self):
        transl = ""
        for t in self.basecalltranslation_set.all():
            transl += ( t.__str__() + '<br>' )
        return mark_safe(transl)


    class Meta:
        verbose_name = _('Grunneining')
        verbose_name_plural = _('Grunneiningar')




class BaseCallTranslation(PolymorphicModel):
    base_master = models.ForeignKey(BaseClass)
    language = models.CharField(max_length=4, choices=settings.LANGUAGES)
    name = models.CharField(_('Heiti'), max_length=250, default='')
    description = models.TextField(_('Ágrip'), blank=True)
    details = models.TextField(_('Nánar'), blank=True)
    note = models.TextField(_('Minnispunktar'), blank=True)

    def __str__(self):
        return "%s: %s" % (self.language, self.name)

    class Meta:
        verbose_name = _('Tungumál')
        verbose_name_plural = _('Tungumál')



class EntityType(BaseClass):
    ordering = models.PositiveIntegerField(default=1, blank=False, null=False)
    icon = models.CharField(max_length=40, default="marker", choices=ICONS)

    class Meta(object):
        ordering = ('ordering',)
        verbose_name = _('Tegund')
        verbose_name_plural = _('Tegundir')

    def admin_icon(self):
        return mark_safe('<img src="/static/olfus/icons/%s-15.svg">' % self.icon )



class HelpCategory(BaseClass):
    ordering = models.PositiveIntegerField(default=1, blank=False, null=False)

    class Meta(object):
        ordering = ('ordering',)
        verbose_name = _('Upplýsingaflokkur')
        verbose_name_plural = _('Upplýsingaflokkar')


class Help(BaseClass):
    ordering = models.PositiveIntegerField(default=1, blank=False, null=False)
    # category = models.CharField(max_length=200, null=True, blank=True)
    category = models.ManyToManyField(HelpCategory, blank=True)
    help_image = FilerImageField(
        null=True,
        blank=True,
        related_name="help_image",
    )
    help_location = models.PointField(srid=4326, null=True, blank=True)

    class Meta(object):
        ordering = ('ordering',)
        verbose_name = _('Upplýsingar')
        verbose_name_plural = _('Upplýsingar')


class Person(BaseClass):
    born = models.DateField(blank=True, null=True)
    died = models.DateField(blank=True, null=True)
    portrait = FilerImageField(
        null=True,
        blank=True,
        related_name="person_portrait",
    )
    other_document = models.ManyToManyField("Document", blank=True, related_name='other_document')
    other_image = models.ManyToManyField("Image", blank=True, related_name='other_image')

    class Meta:
        verbose_name = _('Einstaklingur')
        verbose_name_plural = _('Einstaklingar')


class Institution(BaseClass):
    entity_type = models.ForeignKey("EntityType")
    on_service_list = models.BooleanField(default=True)
    established = models.DateField(blank=True, null=True)
    street_address = models.CharField(max_length=200, null=True, blank=True)
    post_code = models.CharField(max_length=200, null=True, blank=True)
    other_document = models.ManyToManyField("Document", blank=True, related_name='institution_other_document')
    other_image = models.ManyToManyField("Image", blank=True, related_name='institution_other_image')
    location = models.PointField(srid=4326, null=True, blank=True)
    phone = models.CharField(max_length=10, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    logo = FilerImageField(
        null=True,
        blank=True,
        related_name="institution_logo",
    )

    @property
    def icon(self):
        if self.entity_type:
            return self.entity_type.icon

    @property
    def geopoint(self):
        if self.location:
            return self.location.json


    @property
    def logo_image(self):
        if self.logo:
            thumbnailer = get_thumbnailer(self.logo)
            thumbnail_options = {'crop': True}
            thumbnail_options.update({'size': (64, 64)})
            path =  thumbnailer.get_thumbnail(thumbnail_options)
            return mark_safe( '<img src="/media/filer_thumbnails/%s">' % path )
        return ''



    @property
    def logo_display(self):
        if self.logo:
            thumbnailer = get_thumbnailer(self.logo)
            thumbnail_options = {'crop': False}
            thumbnail_options.update({'size': (0, 60)})
            return thumbnailer.get_thumbnail(thumbnail_options)
        return ''

    class Meta:
        ordering = ('name_global',)
        verbose_name = _('Fyrirtæki eða stofnun')
        verbose_name_plural = _('Fyrirtæki eða stofnanir')



class Document(BaseClass):
    file = FilerFileField(
        null=True,
        blank=True,
        related_name="document_file"
    )
    # connected = models.ManyToManyField("BaseClass", blank=True, related_name='document_connection')
    document_date = models.DateField(blank=True, null=True)
    creator = models.ForeignKey(Person, blank=True, null=True)
    by_institution = models.ForeignKey(Institution, blank=True, null=True)

    class Meta:
        verbose_name = _('Skjal')
        verbose_name_plural = _('Skjöl')


class Image(BaseClass):
    image_file = FilerImageField(
        null=True,
        blank=True,
        related_name="image_file"
    )
    # connected = models.ManyToManyField("BaseClass", blank=True, related_name='image_connection' )
    photographer = models.ForeignKey("Person", blank=True, null=True)
    photographed_date = models.DateField(blank=True, null=True)
    image_institution = models.ForeignKey(Institution, blank=True, null=True)
    image_location = models.PointField(srid=4326, null=True, blank=True)

    @property
    def admin_image(self):
        if self.image_file:
            return mark_safe('<img src="%s">' % self.image_file.icons['64'])

    @property
    def gallery_display(self):
        if self.image_file:
            thumbnailer = get_thumbnailer(self.image_file)
            thumbnail_options = {'crop': False}
            thumbnail_options.update({'size': (0, 120)})
            return thumbnailer.get_thumbnail(thumbnail_options)
        return None

    @property
    def image_display(self):
        if self.image_file:
            thumbnailer = get_thumbnailer(self.image_file)
            thumbnail_options = {'crop': False, 'upscale': False}
            thumbnail_options.update({'size': (1200, 800)})
            return thumbnailer.get_thumbnail(thumbnail_options)
        return None

    class Meta:
        verbose_name = _('Mynd')
        verbose_name_plural = _('Myndir')


class House(BaseClass):
    number = models.SmallIntegerField(null=True, blank=True)
    service_house = models.BooleanField(default=False)
    house_location = models.PointField(srid=4326)
    owned_by = models.ManyToManyField(Institution, blank=True)
    special_needs = models.NullBooleanField(default=False)
    main_image = models.ForeignKey(Image, blank=True, null=True, related_name='house_image')
    images = models.ManyToManyField(Image, blank=True)

    class Meta:
        ordering = ['number']
        verbose_name = _('Hús')
        verbose_name_plural = _('Hús')


    def owners(self):
        owlist = "<ul>"
        for o in self.owned_by.all():
            owlist += '<li>'  + o.name_global + '</li> '

        owlist += "</ul>"
        return mark_safe(owlist)

HIKE_SCALE= [
    (1, _('Létt')),
    (2, _('Nokkuð létt')),
    (3, _('Miðlungs')),
    (4, _('Nokuð erfið')),
    (5, _('Erfið')),
]



class HikePath(BaseClass):
    path = models.LineStringField(srid=4326)
    main_image = models.ForeignKey(Image, blank=True, null=True, related_name='hike_image')
    images = models.ManyToManyField(Image, blank=True)
    path_color = ColorField(default='#FF0000')
    difficulty = models.SmallIntegerField(default=2, choices=HIKE_SCALE )

    @property
    def path_length(self):
        pth = self.path
        pth.transform(3035)
        return pth.length

    class Meta:
        verbose_name = _('Gönguslóð')
        verbose_name_plural = _('Gönguslóðir')


ANNOUNCE_TYPE = [
    ('default', _('white - Normal')),
    ('primary', _('Blue - Normal')),
    ('succes', _('Green - OK')),
    ('warning', _('Yellow - warning')),
    ('danger', _('Red - danger')),
]


class Announcement(BaseClass):
    active_date = models.DateTimeField(blank=True, null=True)
    inactive_date = models.DateTimeField(blank=True, null=True)
    houses = models.ManyToManyField(House, blank=True)
    area = models.MultiPolygonField(srid=4326, null=True, blank=True)
    alert_type = models.CharField(max_length=50, default='default', choices=ANNOUNCE_TYPE)


    def in_action(self):
        curr = timezone.now()
        if self.active_date and self.inactive_date:
            if self.active_date <= curr and self.inactive_date >= curr:
                return True
        return False

    class Meta:
        verbose_name = _('Tilkynning')
        verbose_name_plural = _('Tilkynningar')



class History(BaseClass):
    period_from_date = models.DateField(blank=True, null=True)
    period_to_date = models.DateField(blank=True, null=True)
    actual_date = models.DateField(blank=True, null=True)
    show_full_date = models.BooleanField(default=True)
    people = models.ManyToManyField(Person, blank=True)
    institutions = models.ManyToManyField(Institution, blank=True)
    main_image = models.ForeignKey(Image, blank=True, null=True, related_name='main_image')
    images = models.ManyToManyField(Image, blank=True)
    documents = models.ManyToManyField(Document, blank=True)
    history_location = models.PointField(srid=4326, null=True, blank=True)

    class Meta(object):
        ordering = ('-actual_date','-period_from_date')
        verbose_name = _('Sögustubbur')
        verbose_name_plural = _('Sögustubbar')


        # @property
        # def id(self):
        #     return self.filer.file.id
        #
        # @property
        # def owner(self):
        #     return self.baseclass.owner
        #
        # @property
        # def id(self):
        #     return self.filer.file.id
        #
        #
        # class Meta:
        #     # You must define a meta with en explicit app_label
        #     app_label = 'artworld'


class Settings(BaseClass):
    # name = models.CharField(max_length=200, default=_('Stilling - t.d. vetur eða sumar'))
    head_image = models.ManyToManyField(Image, blank=True)
    # title = models.CharField(max_length=24, default=_('Velkomin í Ölfusborgir'))
    # motto = models.CharField(max_length=50, default=_('Kyrrð og ró'))
    # active = models.BooleanField(default=False)
    default = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Stillingar')
        verbose_name_plural = _('Stillingar')


class Teaser(BaseClass):
    icon = models.CharField(max_length=100, null=True, blank=True, choices=FAICONS)
    ordering = models.PositiveIntegerField(default=1, blank=False, null=False)

    class Meta(object):
        ordering = ('ordering',)
        verbose_name = _('Stikla')
        verbose_name_plural = _('Stiklur')




class MessageImage(models.Model):
    name = models.CharField(max_length=200, default='missing_name')
    image = models.ImageField(upload_to='message_image/%Y/%m/')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class MessageFile(models.Model):
    name = models.CharField(max_length=200, default='missing_name')
    file = models.ImageField(upload_to='message_file/%Y/%m/')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name




class Message(models.Model):
    created = models.DateTimeField(_("Skráð"), auto_now_add=True)
    modified = models.DateTimeField(_("Breytt"), auto_now=True)
    title = models.CharField(_("Varðandi"), max_length=200, null=True, blank=True)
    house = models.ForeignKey(House, blank=True, null=True, verbose_name=_("Hús"),
                             help_text=_('Ef skilaboðin varða tiltekið hús veldu það af listanum') )
    sender_name = models.CharField(max_length=200, null=True, blank=True, verbose_name=_("Nafn sendanda"),)
    sender_phone = models.CharField(max_length=200, null=True, blank=True, verbose_name=_("Sími sendanda"),)
    sender_email = models.EmailField(max_length=200, null=True, blank=True, verbose_name=_("Netfang sendanda"),)
    message = models.TextField(null=True, blank=True, verbose_name=_("Skilaboð"),)

    picture = FilerImageField(
        null=True,
        blank=True,
        related_name="message_picture",
        # upload_to='message_pictures/',
    )

    private_note = models.TextField(null=True, blank=True, verbose_name=_("Minnispunktar"),)
    is_done = models.BooleanField(default=False, verbose_name=_("Afgreitt"),)
    rating = models.FloatField(default=0.0, verbose_name=_("Einkunn"),)

    images = models.ManyToManyField(MessageImage, blank=True)

    class Meta:
        verbose_name = _('Skilaboð')
        verbose_name_plural = _('Skilaboð')

    def image_count(self):
        return self.images.count()

    def images_list(self):
        ll = ''
        for img in self.images.all():
            ll += '<a href="/media/%s" target="_blank"><img src="/media/%s" style="height:80px; width: auto;"></a> -  ' % (
                img.image,
                img.image,
            )

        return mark_safe(ll)


class TechNote(models.Model):
    created = models.DateTimeField(_("Skráð"), auto_now_add=True)
    modified = models.DateTimeField(_("Breytt"), auto_now=True)
    wt_subject = models.CharField(max_length=300, blank=True)
    wt_name = models.CharField(max_length=300, blank=True)
    wt_email = models.CharField(max_length=300, blank=True)
    wt_note = models.TextField(blank=True)
    wt_screenshot = models.ImageField( upload_to='technotes')


